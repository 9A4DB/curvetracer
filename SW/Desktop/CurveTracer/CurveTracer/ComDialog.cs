﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO.Ports;

namespace CurveTracer
{
    public partial class ComDialog : Form
    {
        SerialPort _com;
        public ComDialog(SerialPort com)
        {
            InitializeComponent();
            _com = com;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this._com.PortName = ((COMPortInfo)cbxControllerPort.SelectedItem).Name;
            this.Close();
        }
    }
}
