﻿namespace CurveTracer
{
    partial class CalDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalMode = new System.Windows.Forms.Button();
            this.btnNormalMode = new System.Windows.Forms.Button();
            this.rbtCalPlus = new System.Windows.Forms.RadioButton();
            this.rbtCalZero = new System.Windows.Forms.RadioButton();
            this.rbtCalMinus = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.udSwing = new System.Windows.Forms.NumericUpDown();
            this.udNullPoint = new System.Windows.Forms.NumericUpDown();
            this.btnDone = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.udSwing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNullPoint)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCalMode
            // 
            this.btnCalMode.Location = new System.Drawing.Point(11, 11);
            this.btnCalMode.Margin = new System.Windows.Forms.Padding(2);
            this.btnCalMode.Name = "btnCalMode";
            this.btnCalMode.Size = new System.Drawing.Size(104, 24);
            this.btnCalMode.TabIndex = 0;
            this.btnCalMode.Text = "Calibration Mode";
            this.btnCalMode.UseVisualStyleBackColor = true;
            this.btnCalMode.Click += new System.EventHandler(this.btnCalMode_Click);
            // 
            // btnNormalMode
            // 
            this.btnNormalMode.Location = new System.Drawing.Point(119, 11);
            this.btnNormalMode.Margin = new System.Windows.Forms.Padding(2);
            this.btnNormalMode.Name = "btnNormalMode";
            this.btnNormalMode.Size = new System.Drawing.Size(113, 24);
            this.btnNormalMode.TabIndex = 1;
            this.btnNormalMode.Text = "Normal Mode";
            this.btnNormalMode.UseVisualStyleBackColor = true;
            this.btnNormalMode.Click += new System.EventHandler(this.btnNormalMode_Click);
            // 
            // rbtCalPlus
            // 
            this.rbtCalPlus.AutoSize = true;
            this.rbtCalPlus.Location = new System.Drawing.Point(11, 50);
            this.rbtCalPlus.Margin = new System.Windows.Forms.Padding(2);
            this.rbtCalPlus.Name = "rbtCalPlus";
            this.rbtCalPlus.Size = new System.Drawing.Size(50, 17);
            this.rbtCalPlus.TabIndex = 2;
            this.rbtCalPlus.Tag = "1";
            this.rbtCalPlus.Text = "+10V";
            this.rbtCalPlus.UseVisualStyleBackColor = true;
            this.rbtCalPlus.CheckedChanged += new System.EventHandler(this.rbtGroupLevel_CheckedChanged);
            // 
            // rbtCalZero
            // 
            this.rbtCalZero.AutoSize = true;
            this.rbtCalZero.Checked = true;
            this.rbtCalZero.Location = new System.Drawing.Point(11, 72);
            this.rbtCalZero.Margin = new System.Windows.Forms.Padding(2);
            this.rbtCalZero.Name = "rbtCalZero";
            this.rbtCalZero.Size = new System.Drawing.Size(38, 17);
            this.rbtCalZero.TabIndex = 3;
            this.rbtCalZero.TabStop = true;
            this.rbtCalZero.Tag = "0";
            this.rbtCalZero.Text = "0V";
            this.rbtCalZero.UseVisualStyleBackColor = true;
            this.rbtCalZero.CheckedChanged += new System.EventHandler(this.rbtGroupLevel_CheckedChanged);
            // 
            // rbtCalMinus
            // 
            this.rbtCalMinus.AutoSize = true;
            this.rbtCalMinus.Location = new System.Drawing.Point(11, 94);
            this.rbtCalMinus.Margin = new System.Windows.Forms.Padding(2);
            this.rbtCalMinus.Name = "rbtCalMinus";
            this.rbtCalMinus.Size = new System.Drawing.Size(47, 17);
            this.rbtCalMinus.TabIndex = 4;
            this.rbtCalMinus.Tag = "-1";
            this.rbtCalMinus.Text = "-10V";
            this.rbtCalMinus.UseVisualStyleBackColor = true;
            this.rbtCalMinus.CheckedChanged += new System.EventHandler(this.rbtGroupLevel_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 52);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Null Point";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 96);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Swing";
            // 
            // udSwing
            // 
            this.udSwing.Location = new System.Drawing.Point(142, 94);
            this.udSwing.Margin = new System.Windows.Forms.Padding(2);
            this.udSwing.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
            this.udSwing.Name = "udSwing";
            this.udSwing.Size = new System.Drawing.Size(90, 20);
            this.udSwing.TabIndex = 17;
            this.udSwing.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.udSwing.ValueChanged += new System.EventHandler(this.udSwing_ValueChanged);
            // 
            // udNullPoint
            // 
            this.udNullPoint.Location = new System.Drawing.Point(142, 50);
            this.udNullPoint.Margin = new System.Windows.Forms.Padding(2);
            this.udNullPoint.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
            this.udNullPoint.Name = "udNullPoint";
            this.udNullPoint.Size = new System.Drawing.Size(90, 20);
            this.udNullPoint.TabIndex = 18;
            this.udNullPoint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.udNullPoint.ValueChanged += new System.EventHandler(this.udNullPoint_ValueChanged);
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(80, 128);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(75, 23);
            this.btnDone.TabIndex = 19;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // CalDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(249, 162);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.udNullPoint);
            this.Controls.Add(this.udSwing);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbtCalMinus);
            this.Controls.Add(this.rbtCalZero);
            this.Controls.Add(this.rbtCalPlus);
            this.Controls.Add(this.btnNormalMode);
            this.Controls.Add(this.btnCalMode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CalDialog";
            this.Text = "Calibration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CalDialog_FormClosing);
            this.Shown += new System.EventHandler(this.CalDialog_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.udSwing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNullPoint)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalMode;
        private System.Windows.Forms.Button btnNormalMode;
        private System.Windows.Forms.RadioButton rbtCalPlus;
        private System.Windows.Forms.RadioButton rbtCalZero;
        private System.Windows.Forms.RadioButton rbtCalMinus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown udSwing;
        private System.Windows.Forms.NumericUpDown udNullPoint;
        private System.Windows.Forms.Button btnDone;
    }
}