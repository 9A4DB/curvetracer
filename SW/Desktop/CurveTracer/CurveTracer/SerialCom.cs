﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.IO.Ports;

namespace CurveTracer
{
    public class SerialCom : SerialPort
    {
        public SerialCom() {}

        public delegate void LineSentHandler(string line);
        public event LineSentHandler LineSent;

        public delegate void ResponseHandler(CTResponse commanddata);
        public event ResponseHandler ResponseReceived;

        CancellationTokenSource ResponseReader_ts;
        CancellationToken ResponseReader_ct;

        CancellationTokenSource SendCommand_ts;
        CancellationToken SendCommand_ct;

        int _bufflen;
        bool _commandSemaphore = false; // Determine if a command allowed to sent through the connection
        public void StartCommandRead(int bufflen)
        {
            _bufflen = bufflen;
            ResponseReader_ts = new CancellationTokenSource();
            ResponseReader_ct = ResponseReader_ts.Token;
            Task.Factory.StartNew((Action)ResponseReadWorker);

            SendCommand_ts = new CancellationTokenSource();
            SendCommand_ct = SendCommand_ts.Token;
            Task.Factory.StartNew((Action)SendCommandWorker);
        }

        public void StopCommandRead()
        {
            ResponseReader_ts.Cancel();
        }

        void ResponseReadWorker()
        {
            int buffpos = 0;
            byte[] buff = new byte[_bufflen];
            int currentbyte = -1;
            bool EndOFResponse = false;
            CTResponse response;
            while (!ResponseReader_ct.IsCancellationRequested)
            {
 //               try
 //               {
                if(buffpos == 0)
                {
                    _commandSemaphore = true;
                }
                try
                {
                    currentbyte = base.BaseStream.ReadByte();
                }
                catch(System.IO.IOException err)
                {
                    StopCommandRead();
                    if (this.IsOpen)
                    {
                        this.Close();
                    }
                    currentbyte = -1;
                }

                if (currentbyte > -1)
                {
                    _commandSemaphore = false;
                    buff[buffpos] = (byte)currentbyte;
                    // check the bytes at the end of actually stored data, if it equals to the newline character
                    if (buffpos >= base.NewLine.Length - 1)
                    {
                        for (int i = 0; i < base.NewLine.Length; i++)
                        {
                            EndOFResponse = base.NewLine[base.NewLine.Length - i - 1] == buff[buffpos - i];
                            if (!EndOFResponse)
                            {
                                break;
                            }
                        }
                    }
                    // at the end of the response (newline) call the command processor
                    if (EndOFResponse)
                    {
                        _commandSemaphore = true;
                        Array.Resize<byte>(ref buff, buffpos - base.NewLine.Length + 1);
                        response = new CTResponse(buff);
                        ResponseReceived(response);
                        buff = new byte[_bufflen];
                        buffpos = 0;
                    }
                    else
                    {
                        buffpos++;
                    }
                }
                else
                {
                    _commandSemaphore = true;
                    // else end of stream. Menans connection closed???
                }
 //               }
//                catch (Exception err)
//                {
//                    // Add loging
//                }
            }
        }

        void SendCommandWorker()
        {
            CTCommand currentCommand;
            while(!SendCommand_ct.IsCancellationRequested)
            {
                if (_sendqueue.Count > 0 && _commandSemaphore)
                {
                    currentCommand = _sendqueue.Dequeue();
                    if (currentCommand != null)
                    {
                        WriteLine(currentCommand.Serialize());
                    }
                    Thread.Sleep(500);
                }
            }
        }


        public new void WriteLine(string text)
        {
            if (LineSent != null)
            {
                LineSent(text);
            }
            base.WriteLine(text);
        }

        private Queue<CTCommand> _sendqueue = new Queue<CTCommand>();
        public void SendCommand(CTCommand command)
        {
            _sendqueue.Enqueue(command);
            /*
            if (_sendqueue.Count > 0 && !_commandSemaphore)
            {
                _sendqueue.Enqueue(command);
            }
            else
            {
                WriteLine(command.Serialize());
            }
            */
        }

        public void SendCommand(CommandCode code, string param)
        {
            SendCommand(new CTCommand(code, param));
        }
        public void SendCommand(CommandCode code, string[] paramarr)
        {
            SendCommand(new CTCommand(code, paramarr));
        }
        public void SendCommand(CommandCode code)
        {
            SendCommand(new CTCommand(code));
        }

        public CommandCode LastCode
        {
            get
            {
                if(_sendqueue.Count > 0)
                {
                    return _sendqueue.Last().Code;
                }
                else
                {
                    return CommandCode.NULL;
                }
            }
        }
        public int SendQueueLength
        {
            get
            {
                return _sendqueue.Count;
            }

        }
    }



}


