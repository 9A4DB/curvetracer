﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurveTracer
{
    public enum ATT_DUT
    {
        OFF = 0,
        A = 1,
        B = 2,
        ALT = 3
    }
}
