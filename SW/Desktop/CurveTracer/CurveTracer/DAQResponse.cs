﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace CurveTracer
{
    public class DAQResponse : CTResponse
    {
        public DAQResponse() { }
        public DAQResponse(byte[] data)
        {
            DeserializeCOM(data);
            this._enableProcessing = true;
        }

        public DAQResponse(CTResponse ctr)
        {
            this.Params = ctr.Params;
            this.Code = ctr.Code;
            this.BinData = ctr.BinData;
            this._enableProcessing = true;
        }

        private int _maVoltRange = -1;
        private int _maCurrRange = -1;
        private int _attCurrentDUT = -1;
        private int _wfgNumSamples = -1;
        private DAQDataPoint[] _dataPoints = null;

        private bool _dataProcessed = false;
        private bool _frameError = false;
        private bool _enableProcessing = false;

        public int maVoltRange
        {
            get
            {
                if(!_dataProcessed)
                {
                    _ProcessData();
                }
                return _maVoltRange;
            }
        }

        public int maCurrRange
        {
            get
            {
                if (!_dataProcessed)
                {
                    _ProcessData();
                }
                return _maCurrRange;
            }
        }
        public int attCurrentDUT
        {
            get
            {
                if (!_dataProcessed)
                {
                    _ProcessData();
                }
                return _attCurrentDUT;
            }
        }
        public int wfgNumSamples
        {
            get
            {
                if (!_dataProcessed)
                {
                    _ProcessData();
                }
                return _wfgNumSamples;
            }
        }

        public DAQDataPoint this[int index]
        {
            get
            {
                if (!_dataProcessed)
                {
                    _ProcessData();
                }
                if(_dataPoints != null)
                {
                    if(_dataPoints.Length > index)
                    {
                        return _dataPoints[index];
                    }
                }
                return null;
            }
        }

        public bool FrameError
        {
            get
            {
                if (!_dataProcessed)
                {
                    _ProcessData();
                }
                return _frameError;
            }
        }

        private void _ProcessData()
        {
            
            byte[] binDataPoint = new byte[5];
            if (this._enableProcessing)
            {
                _dataProcessed = true;
                _frameError = Code != "a";
                if (_frameError) return;
                if (Params.Length == 4)
                {
                    _frameError = !int.TryParse(Params[0], out this._maVoltRange);
                    if (_frameError) return;
                    _frameError = !int.TryParse(Params[1], out this._maCurrRange);
                    if (_frameError) return;
                    _frameError = !int.TryParse(Params[2], out this._attCurrentDUT);
                    if (_frameError) return;
                    _frameError = !int.TryParse(Params[3], out this._wfgNumSamples);
                    if (_frameError) return;
                }
                else
                {
                    _frameError = true;
                    return;
                }
                
                _frameError = BinData == null;
                if (_frameError) return;
                
                _frameError = (_wfgNumSamples * 5) != BinData.Length;
                if (_frameError) return;
                
                _dataPoints = new DAQDataPoint[_wfgNumSamples];
                for (int i = 0; i < _wfgNumSamples; i++)
                {
                    Array.ConstrainedCopy(BinData, i * 5, binDataPoint, 0, 5);
                    _frameError = !DAQDataPoint.TryParsePoint(binDataPoint, out this._dataPoints[i]);
                    if (_frameError) return;
                }
            }
        }

        private Bitmap _scopeImage;
        private Graphics _scopeGraphics;
        private Pen scopePen;
        private int XScaledSave = -1;
        private int YScaledSave = -1;
        private double yScale;
        private double xScale;

        private void _addPoint(uint x, uint y)
        {
            int XScaled = Convert.ToInt32(x * xScale);
            int YScaled = _scopeImage.Height - 1 - Convert.ToInt32(y * yScale);
            if (XScaled <= 0) XScaled = 0;
            if (XScaled >= _scopeImage.Width - 1) XScaled = _scopeImage.Width - 1;
            if (YScaled <= 0) YScaled = 0;
            if (YScaled >= _scopeImage.Height - 1) YScaled = _scopeImage.Height - 1;
            if (XScaledSave > -1 && YScaledSave > -1)
            {
                _scopeGraphics.DrawLine(scopePen, XScaledSave, YScaledSave, XScaled,  YScaled);
            }
            XScaledSave = XScaled;
            YScaledSave = YScaled;
        }

        public void Render(ref Bitmap bitmap, Color color, Color background)
        {
            xScale = bitmap.Width / 4096.0;
            yScale = bitmap.Height / 4096.0;
            if (!_dataProcessed)
            {
                _ProcessData();
            }
            _scopeImage = bitmap;
            _scopeGraphics = Graphics.FromImage(_scopeImage);
            if(background != Color.Empty)
            {
                _scopeGraphics.Clear(background);
            }
            scopePen = new Pen(color, 1);
            if (!_frameError)
            {
                for (int i = 0; i < _wfgNumSamples; i++)
                {
                    _addPoint(_dataPoints[i].Voltage, _dataPoints[i].Current);
                }
                _scopeGraphics.Flush();
            }
            XScaledSave = -1;
            YScaledSave = -1;
        }
        /*
        public void DAQ_Test_Render(ref Bitmap bitmap, Color color, Color background, ref int y)
        {
            if (!_dataProcessed)
            {
                _ProcessData();
            }
            _scopeImage = bitmap;
            _scopeGraphics = Graphics.FromImage(_scopeImage);
            _scopeGraphics.Clear(background);
            Pen scopePen = new Pen(color, 1);
            Pen currentPen = new Pen(Color.Blue, 1);
            for (int x = 0; x < this._wfgNumSamples; x++)
            {
                _scopeGraphics.FillRectangle(scopePen.Brush, x, _dataPoints[x].Voltage >> 3, 1, 1);
                _scopeGraphics.FillRectangle(currentPen.Brush, x, _dataPoints[x].Current >> 3, 1, 1);
            }
            // _scopeGraphics.DrawLine(scopePen, 0, y, _scopeImage.Width, y);
            _scopeGraphics.Flush();
            y++;
        }
        */
        public void DAQ_Test_Render(ref Bitmap bitmap, Color color, Color background, ref int y)
        {
            double yScale = bitmap.Height / 4096.0;
            double xScale = bitmap.Width / 4096.0;
            double current;
            double voltage;
            if (!_dataProcessed)
            {
                _ProcessData();
            }
            _scopeImage = bitmap;
            _scopeGraphics = Graphics.FromImage(_scopeImage);
            _scopeGraphics.Clear(background);
            Pen scopePen = new Pen(color, 1);
            Pen currentPen = new Pen(Color.Blue, 1);
            for (int x = 0; x < this._wfgNumSamples; x++)
            {
                voltage = _dataPoints[x].Voltage * yScale;
                current = _dataPoints[x].Current * yScale;
                _scopeGraphics.FillRectangle(scopePen.Brush, x, Convert.ToUInt16(voltage), 1, 1);
                _scopeGraphics.FillRectangle(currentPen.Brush, x, Convert.ToUInt16(current), 1, 1);
            }
            // _scopeGraphics.DrawLine(scopePen, 0, y, _scopeImage.Width, y);
            _scopeGraphics.Flush();
            y++;
        }


    }

    public class DAQDataPoint
    {
        public uint Voltage;
        public uint Current;
        public uint PointNumber;

        public DAQDataPoint() { }
        /*
        /// <summary>
        /// Parse the 5 byte serially transfered data
        /// </summary>
        /// <param name="binData">5 byte source data</param>
        public void ParsePoint(byte[] binData)
        {
            PointNumber = (((uint)binData[0] & 0x7F) << 2) | (((uint)binData[1] & 0x60) >> 5);
            Voltage = (((uint)binData[1] & 0x1F) << 7) | ((uint)binData[2] & 0x7F);
            Current = (((uint)binData[3] & 0x7F) << 5) | ((uint)binData[4] & 0x1F);
        }
        */
        public static bool TryParsePoint(byte[] binData, out DAQDataPoint dataPoint)
        {
            dataPoint = new DAQDataPoint();
            if(binData.Length != 5) return false;
            for (UInt16 i = 0; i < 4; i++)
            {
                if (binData[i] < 128) return false;
            }
            if((binData[4] & 0xC0) != 0x40) return false;
            
            dataPoint.PointNumber = (((uint)binData[0] & 0x7F) << 2) | (((uint)binData[1] & 0x60) >> 5);

            dataPoint.Voltage = (((uint)binData[1] & 0x1F) << 7) | ((uint)binData[2] & 0x7F);

            dataPoint.Current = (((uint)binData[3] & 0x7F) << 5) | ((uint)binData[4] & 0x1F);
            /*
            dataPoint.PointNumber = 0;
            dataPoint.Voltage = (uint)(binData[2] & 0x7F);
            dataPoint.Current = 0;
            */
            return true;
        }
    }


}

