/*
 * data_acquisition.c
 *
 *  Created on: 2018. okt. 13.
 *      Author: zoli
 */

#include "stm32f4xx_hal.h"
#include "adc.h"
#include "wfg.h"
#include "CurveTracer.h"
#include "dma_sync.h"
#include "data_acquisition.h"
#include <stdbool.h>
#include "command.h"
#include "SCREEN.h"

uint16_t adc_voltage_data[WFG_MAX_SAMPLES];
uint16_t adc_current_data[WFG_MAX_SAMPLES];
volatile bool adc_ready;
volatile bool adc_data_ready;
// Communication frame enabled - we send data on request only
volatile bool adc_usb_frame_enabled;
volatile bool adc_scr_frame_enabled;

void DAQ_Start()
{
	if(adc_ready)
	{
		HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc_voltage_data,CT_NV_Settings.wfgNumSamples);
		HAL_ADC_Start_DMA(&hadc2, (uint32_t*)adc_current_data,CT_NV_Settings.wfgNumSamples);
		adc_ready = false;
	}
}

void DAQ_Process()
{
	adc_data_ready = true;
}

void _daq_send_data(uint16_t samplenum, uint16_t voltage, uint16_t current)
{
	uint8_t dataarr[5];
	// All of the 7 bit data extended with a 0x80 to avoid conflict with the below 32 ascii control characters

	dataarr[0] = ((samplenum >> 2) & 0x007F) | 0x80;								// upper 7 bit of 9 bit sample num (max 511)
	dataarr[1] = (((samplenum << 5) | ((voltage >> 7) & 0x001F)) & 0x007F) | 0x80;	// lower 2 bit of sample num, upper 5 bit of 12 bit voltage
	dataarr[2] = (voltage & 0x007F) | 0x80;											// lower 7 bit of voltage
	dataarr[3] = ((current >> 5) & 0x007F) | 0x80;									// upper 7 bit of 12 bit current
	dataarr[4] = (current & 0x001F) | 0x40;											// lower 5 bit of current plus the frame closing 0x40
	CMD_Response_Raw(dataarr, 5);
}
void _daq_send_header()
{
	// current amp gain
	// voltage amp gain
	// currently selected DUT
	// number of samples
	CMD_Response(CMD_SEND_DATA_T,CMD_SEND_DATA,CT_V_Settings.maVoltRange,CT_V_Settings.maCurrRange,CT_V_Settings.attCurrentDUT,CT_NV_Settings.wfgNumSamples);
}

void DAQ_Process_Worker()
{
	uint16_t i;
	if(adc_data_ready)
	{
		/*
		if(adc_scr_frame_enabled)
		{
			adc_ready = false;
			SCREEN_Scope_Setup();
			for(i = 0; i < CT_NV_Settings.wfgNumSamples; i++)
			{
				// _daq_send_data(i, 4096 - adc_voltage_data[i], 4096 - adc_current_data[i]);
				SCREEN_Scope_AddPoint(i, 4096 - adc_voltage_data[i], adc_current_data[i]);
			}
			SCREEN_Scope_Display();
			adc_scr_frame_enabled = false;
		}
		*/
		if(adc_usb_frame_enabled)
		{
			adc_ready = false;
			_daq_send_header();
			for(i = 0; i < CT_NV_Settings.wfgNumSamples; i++)
			{
				_daq_send_data(i, 4096 - adc_voltage_data[i], 4096 - adc_current_data[i]);
			}
			CMD_Response_LF();
			adc_usb_frame_enabled = false;
		}
		adc_data_ready = false;
		adc_ready = true;
	}
}


// -----------------------GetData-------------------------------

void DAQ_GetData(uint8_t source)
{
	switch(source)
	{
		case CMD_COM_DEVICE_CON:
			adc_scr_frame_enabled = true;
			break;
		case CMD_COM_DEVICE_USB:
			adc_usb_frame_enabled = true;
			break;
	}
}

/// Command descriptor for WFG_SetFreq

void DAQ_CMD_GetData(uint8_t source, void* args[], uint8_t len)
{
	DAQ_GetData(source);
}

const CMD_CommandTypeDef DAQ_CMD_GetData_Descriptor =
{
	.CommandCode = CMD_CODE_DAQ_GETDATA,
	.NumParams = 0,
	.CommandProcessor = &DAQ_CMD_GetData,
	.ParamArr = NULL
};

void _wfg_DAQ_Register()
{
	CMD_RegisterCommand(DAQ_CMD_GetData_Descriptor);
}

void DAQ_Init()
{
	_wfg_DAQ_Register();

	adc_ready = true;
	adc_data_ready = false;
	adc_usb_frame_enabled = false;
	HAL_ADC_Start(&hadc1);
	HAL_ADC_Start(&hadc2);
}
