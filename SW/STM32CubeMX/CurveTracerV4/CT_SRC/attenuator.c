/*
 * attenuator.c
 *
 *  Created on: 2018. aug. 25.
 *      Author: zoli
 */

#include "stm32f4xx_hal.h"
#include <stdbool.h>
#include "attenuator.h"
#include "MCP230xx.h"
#include "command.h"
#include "CurveTracer.h"

const ATT_ResTypeDef att_preset_res[14] =
{
		{ .gpio = 0x0000, .value = 0.0 },		// Off
		{ .gpio = 0x3000, .value = 10.0 },		// 10 ohm
		{ .gpio = 0x2000, .value = 20.0 },		// 20 ohm
		{ .gpio = 0x0CC0, .value = 50.0 },		// 50 ohm
		{ .gpio = 0x0C00, .value = 100.0 },		// 100 ohm
		{ .gpio = 0x0800, .value = 200.0 },		// 200 ohm
		{ .gpio = 0x0200, .value = 500.0 },		// 500 ohm
		{ .gpio = 0x0100, .value = 1000.0 },	// 1K ohm
		{ .gpio = 0x0020, .value = 2000.0 },	// 2K ohm
		{ .gpio = 0x0010, .value = 5000.0 },	// 5K ohm
		{ .gpio = 0x0008, .value = 10000.0 },	// 10K ohm
		{ .gpio = 0x0004, .value = 20000.0 },	// 20K ohm
		{ .gpio = 0x0002, .value = 50000.0 },	// 50K ohm
		{ .gpio = 0x0001, .value = 100000.0 }	// 100K ohm
};

const double att_res_arr[14] =
{
		100000,
		 50000,
		 20000,
		 10000,
		  5000,
		  2000,
		   200,
		   200,
		  1000,
		   500,
		   200,
		   200,
		    20,
			20
};


uint16_t att_dut_alt_count; // counter for alternating DUT, counting the switching period


///
/// ATT_DUT_Switcher - Switching between the DUTs on sine zero crossing
///
void ATT_DUT_Switcher()
{
	switch(CT_NV_Settings.attDUTSel)
	{
		case ATT_DUT_OFF:
			if(CT_V_Settings.attCurrentDUT != CT_NV_Settings.attDUTSel)
			{
				CT_V_Settings.attCurrentDUT = CT_NV_Settings.attDUTSel;
				MCP23017_WriteW(I2C_ADDR_ATT_IOEXT, CT_NV_Settings.attResistanceSet ^ 0x0FFF);
			}
			break;
		case ATT_DUT_1:
		case ATT_DUT_2:
			if(CT_V_Settings.attCurrentDUT != CT_NV_Settings.attDUTSel)
			{
				CT_V_Settings.attCurrentDUT = CT_NV_Settings.attDUTSel;
				MCP23017_WriteW(I2C_ADDR_ATT_IOEXT, (((uint16_t)1) << (CT_V_Settings.attCurrentDUT + 13)) | (CT_NV_Settings.attResistanceSet ^ 0x0FFF));
			}
			break;
		case ATT_DUT_ALT:
			// counter finished, switch between the DUTs
			if(att_dut_alt_count == 0)
			{
				// reset counter
				att_dut_alt_count = CT_NV_Settings.attDUTaltcycles;
				// if 1 is currently selected, switch to 2, in any other case switch back to 1
				CT_V_Settings.attCurrentDUT = (CT_V_Settings.attCurrentDUT == ATT_DUT_1) ? ATT_DUT_2 : ATT_DUT_1;
				// send out to the physical switch
				MCP23017_WriteW(I2C_ADDR_ATT_IOEXT, (((uint16_t)1) << (CT_V_Settings.attCurrentDUT + 13)) | (CT_NV_Settings.attResistanceSet ^ 0x0FFF));
			}
			else
			{
				att_dut_alt_count--;
			}
			break;
	}
}


// -----------------------SetAltCycles-------------------------------
void ATT_SetAltCycles(uint8_t cycles)
{
	CT_NV_Settings.attDUTaltcycles = cycles;
	CT_NV_Save();
}

void ATT_CMD_SetAltCycles(uint8_t source, void* args[], uint8_t len)
{
	ATT_SetAltCycles(*((uint8_t*)args[0]));
}

const uint8_t ATT_CMD_SetAltCycles_Params[1] = {CMD_TYPE_UINT8};

const CMD_CommandTypeDef ATT_CMD_SetAltCycles_Descriptor =
{
	.CommandCode = CMD_CODE_ATT_SETALTCYCLES,
	.NumParams = 1,
	.CommandProcessor = &ATT_CMD_SetAltCycles,
	.ParamArr = ATT_CMD_SetAltCycles_Params
};



// -----------------------SelRes-------------------------------
double ATT_SelRes(uint8_t index)
{
	CT_NV_Settings.attResistanceSet = att_preset_res[index].gpio;
	CT_NV_Save();
	CT_V_Settings.attResistance = att_preset_res[index].value;
	MCP23017_WriteW(I2C_ADDR_ATT_IOEXT, (((uint16_t)1) << (CT_V_Settings.attCurrentDUT + 13)) | (CT_NV_Settings.attResistanceSet ^ 0x0FFF));
	ATT_GetRes();
	return CT_V_Settings.attResistance;
}

void ATT_CMD_SelRes(uint8_t source, void* args[], uint8_t len)
{
	ATT_SelRes(*((uint8_t*)args[0]));
}

const uint8_t ATT_CMD_SelRes_Params[1] = {CMD_TYPE_UINT8};

const CMD_CommandTypeDef ATT_CMD_SelRes_Descriptor =
{
	.CommandCode = CMD_CODE_ATT_SELRES,
	.NumParams = 1,
	.CommandProcessor = &ATT_CMD_SelRes,
	.ParamArr = ATT_CMD_SelRes_Params
};

// -----------------------SelDUT-------------------------------
///
///	Select Device under test
/// parameters:
///		DUT - Device under test ID: Off, 1, 2, Alternating mode
///		alt_cycles - Number of full sine cycles used on a single DUT before the switch occurs
///

void ATT_SelDUT(uint8_t DUT,uint16_t alt_cycles)
{
	CT_NV_Settings.attDUTSel = DUT;

	CT_NV_Settings.attDUTaltcycles = alt_cycles;
	CT_NV_Save();
	ATT_GetDUT();
}

void ATT_CMD_SelDUT(uint8_t source, void* args[], uint8_t len)
{
	ATT_SelDUT(*((uint8_t*)args[0]),*((uint16_t*)args[1]));
}

const uint8_t ATT_CMD_SelDUT_Params[2] = {CMD_TYPE_UINT8, CMD_TYPE_UINT16};

const CMD_CommandTypeDef ATT_CMD_SelDUT_Descriptor =
{
	.CommandCode = CMD_CODE_ATT_SELDUT,
	.NumParams = 2,
	.CommandProcessor = &ATT_CMD_SelDUT,
	.ParamArr = ATT_CMD_SelDUT_Params
};

// -----------------------GetRes-------------------------------
void ATT_GetRes()
{
	int i;
	bool isFirstValue = true;
	double res_value = -1;
	for(i=0;i<14;i++)
	{
		if(CT_NV_Settings.attResistanceSet & (1 << i))
		{
			if(isFirstValue)
			{
				res_value = att_res_arr[i];
				isFirstValue = false;
			}
			else
			{
				res_value = (res_value * att_res_arr[i]) / (res_value + att_res_arr[i]);
			}
		}
	}
	if(res_value > 0)
	{
		CMD_Response(CMD_SEND_RES_T, CMD_SEND_RES, res_value);
	}
	else
	{
		CMD_Response_Empty(CMD_SEND_RES_OFF, CMD_SEND_RES);
	}
}

/// Command descriptor for ATT_GetRes

void ATT_CMD_GetRes(uint8_t source, void* args[], uint8_t len)
{
	ATT_GetRes();
}

const CMD_CommandTypeDef ATT_CMD_GetRes_Descriptor =
{
	.CommandCode = CMD_CODE_ATT_GETRES,
	.NumParams = 0,
	.CommandProcessor = &ATT_CMD_GetRes,
	.ParamArr = NULL
};
// -----------------------GetDUT-------------------------------
// Respond: - Selection (1, 2, ALT, Off)
//			- Current state (which DUT is currently on)
//			- Alternating cycle

void ATT_GetDUT()
{
	CMD_Response(CMD_SEND_DUT_T, CMD_SEND_DUT, CT_NV_Settings.attDUTSel, CT_V_Settings.attCurrentDUT, CT_NV_Settings.attDUTaltcycles);
}

void WFG_CMD_GetDUT(uint8_t source, void* args[], uint8_t len)
{
	ATT_GetDUT();
}

const CMD_CommandTypeDef ATT_CMD_GetDUT_Descriptor =
{
	.CommandCode = CMD_CODE_ATT_GETDUT,
	.NumParams = 0,
	.CommandProcessor = &WFG_CMD_GetDUT,
	.ParamArr = NULL
};


void _att_CMD_Register()
{
	CMD_RegisterCommand(ATT_CMD_SelRes_Descriptor);
	CMD_RegisterCommand(ATT_CMD_SelDUT_Descriptor);
	CMD_RegisterCommand(ATT_CMD_GetRes_Descriptor);
	CMD_RegisterCommand(ATT_CMD_SetAltCycles_Descriptor);
	CMD_RegisterCommand(ATT_CMD_GetDUT_Descriptor);
}

void ATT_Init()
{
	_att_CMD_Register();
	MCP23017_Init(I2C_ADDR_ATT_IOEXT);
	ATT_DUT_Switcher();
	MCP23017_WriteW(I2C_ADDR_ATT_IOEXT, (((uint16_t)1) << (CT_V_Settings.attCurrentDUT + 13)) | (CT_NV_Settings.attResistanceSet ^ 0x0FFF));

//	MCP23017_Init(I2C_ADDR_ATT_IOEXT);
//	MCP23017_WriteW(I2C_ADDR_ATT_IOEXT,0x0001);
}
