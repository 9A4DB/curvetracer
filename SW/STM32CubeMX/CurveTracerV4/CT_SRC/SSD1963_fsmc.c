/*
 * SSD1963_fsmc.c
 *
 *  Created on: 2018. nov. 26.
 *      Author: zoli
 */

#include <stdint.h>
#include "SSD1963_fsmc.h"
#include "stm32f4xx_hal.h"
#include "lvgl.h"
#include "../lv_conf.h"



// library functions

void SSD1963_Flush(int32_t x1, int32_t y1, int32_t x2, int32_t y2, const lv_color_t * color_p)
{
	SSD1963_Map(x1, y1, x2, y2, color_p);
	lv_flush_ready();
}

void SSD1963_Fill(int32_t x1, int32_t y1, int32_t x2, int32_t y2, lv_color_t color)
{
    // Return if the area is out the screen
    if(x2 < 0) return;
    if(y2 < 0) return;
    if(x1 > SSD1963_WIDTH - 1) return;
    if(y1 > SSD1963_HEIGHT - 1) return;

    // Truncate the area to the screen
    uint16_t act_x1 = x1 < 0 ? 0 : x1;
    uint16_t act_y1 = y1 < 0 ? 0 : y1;
    uint16_t act_x2 = x2 > SSD1963_WIDTH - 1 ? SSD1963_WIDTH - 1 : x2;
    uint16_t act_y2 = y2 > SSD1963_HEIGHT - 1 ? SSD1963_HEIGHT - 1 : y2;

    uint16_t color16 = lv_color_to16(color);
    uint32_t size = (act_x2 - act_x1 + 1) * (act_y2 - act_y1 + 1);
    uint32_t i;

	_SSD1963_SetArea(x1, y1, x2, y2);
	SSD1963_REG = SSD1963_CMD_WRITE_MEMORY_START;
	for(i = 0; i < size; i++)
	{
		SSD1963_RAM = color16;
	}
}

void SSD1963_Map(int32_t x1, int32_t y1, int32_t x2, int32_t y2, const lv_color_t * color_p)
{
    // Return if the area is out the screen
    if(x2 < 0) return;
    if(y2 < 0) return;
    if(x1 > SSD1963_WIDTH - 1) return;
    if(y1 > SSD1963_HEIGHT - 1) return;

    // Truncate the area to the screen
    uint16_t act_x1 = x1 < 0 ? 0 : x1;
    uint16_t act_y1 = y1 < 0 ? 0 : y1;
    uint16_t act_x2 = x2 > SSD1963_WIDTH - 1 ? SSD1963_WIDTH - 1 : x2;
    uint16_t act_y2 = y2 > SSD1963_HEIGHT - 1 ? SSD1963_HEIGHT - 1 : y2;

//    uint16_t color16 = lv_color_to16(color);
    uint32_t size = (act_x2 - act_x1 + 1) * (act_y2 - act_y1 + 1);
    uint32_t i;

	_SSD1963_SetArea(x1, y1, x2, y2);
	SSD1963_REG = SSD1963_CMD_WRITE_MEMORY_START;
	for(i = 0; i < size; i++)
	{
		SSD1963_RAM = lv_color_to16(color_p[i]);
	}
}

// Helper functions

void _SSD1963_SetArea(uint16_t x1,uint16_t y1, uint16_t x2, uint16_t y2)
{

	SSD1963_REG = SSD1963_CMD_SET_COLUMN_ADDRESS;
	SSD1963_RAM = x1 >> 8;
	SSD1963_RAM = x1 & 0x00FF;
	SSD1963_RAM = x2 >> 8;
	SSD1963_RAM = x2 & 0x00FF;
	SSD1963_REG = SSD1963_CMD_SET_PAGE_ADDRESS;
	SSD1963_RAM = y1 >> 8;
	SSD1963_RAM = y1 & 0x00FF;
	SSD1963_RAM = y2 >> 8;
	SSD1963_RAM = y2 & 0x00FF;
	return;
}


// Setup functions

void SSD1963_Init()
{
	HAL_Delay(2000);
	// Setup pixel clock (120MHz)
	_SSD1963_PLL_Enable(0x23, 0x02);
	SSD1963_REG = SSD1963_CMD_SOFT_RESET;
	HAL_Delay(500);
	_SSD1963_Set_Lshift(0x3FFFF);

	// Set LCD Size
	_SSD1963_LCD_Mode(SSD1963_MODE_BITS_24, SSD1963_WIDTH, SSD1963_HEIGHT);

	SSD1963_REG = SSD1963_CMD_SET_ADDRESS_MODE;

	// Set rotation
	SSD1963_RAM = SSD1963_AM_HORI_FLIP | SSD1963_AM_VERT_FLIP;

	// Set pixel color scheme
	SSD1963_REG = SSD1963_CMD_SET_PIXEL_DATA_INTERFACE;
	SSD1963_RAM = SSD1963_PDI_16_565;

	// Clear screen
	SSD1963_Fill(0, 0, SSD1963_WIDTH - 1, SSD1963_HEIGHT - 1, LV_COLOR_BLACK);

	// Switch on
	SSD1963_REG = SSD1963_CMD_SET_DISPLAY_ON;
}


void _SSD1963_PLL_Enable(uint8_t multiplier, uint8_t divider)
{
	// Setup PLL
	SSD1963_REG = SSD1963_CMD_SET_PLL_MN;
	SSD1963_RAM = multiplier;
	SSD1963_RAM = divider;
	SSD1963_RAM = 0x04;

	// Enable PLL Clock generation
	SSD1963_REG = SSD1963_CMD_SET_PLL;
	SSD1963_RAM = SSD1963_PLL_ENABLE;
	// Wait for stabilize
	HAL_Delay(10);
	// Lock PLL
	SSD1963_REG = SSD1963_CMD_SET_PLL;
	SSD1963_RAM = SSD1963_PLL_LOCK | SSD1963_PLL_ENABLE;
	HAL_Delay(10);
}


void _SSD1963_LCD_Mode(uint16_t dw,uint16_t width, uint16_t height)
{
	SSD1963_REG = SSD1963_CMD_SET_LCD_MODE;
	SSD1963_RAM = dw;
	SSD1963_RAM = 0x00;	// TFT Mode
	SSD1963_RAM = (width-1) >> 8;
	SSD1963_RAM = (width-1) & 0x00FF;
	SSD1963_RAM = (height-1) >> 8;
	SSD1963_RAM = (height-1) & 0x00FF;
	SSD1963_RAM = 0; // RGB sequence

	SSD1963_REG = SSD1963_CMD_SET_HORI_PERIOD;
	SSD1963_RAM = (width + 128) >> 8;		// HT_H
	SSD1963_RAM = (width + 128) & 0x00FF;	// HT_L
	SSD1963_RAM = 0;						// HPS_H
	SSD1963_RAM = 0x2E;						// HPS_L
	SSD1963_RAM = 0x30;						// HPW
	SSD1963_RAM = 0;						// LPS_H
	SSD1963_RAM = 0x0F;						// LPS_L
	SSD1963_RAM = 0;						// LPSPP

	SSD1963_REG = SSD1963_CMD_SET_VERT_PERIOD;
	SSD1963_RAM = (height + 45) >> 8;		// VT_H
	SSD1963_RAM = (height + 45) & 0x00FF;	// VT_L
	SSD1963_RAM = 0;						// VPS_H
	SSD1963_RAM = 0x10;						// VPS_L
	SSD1963_RAM = 0x10;						// VPW
	SSD1963_RAM = 0;						// FPS_H
	SSD1963_RAM = 0x08;						// FPS_L

}

void _SSD1963_Set_Lshift(uint32_t value)
{
	SSD1963_REG = SSD1963_CMD_SET_LSHIFT_FREQ;
	SSD1963_RAM = (value >> 16) & 0x0000000F;
	SSD1963_RAM = (value >> 8)  & 0x000000FF;
	SSD1963_RAM = value & 0x000000FF;
}
