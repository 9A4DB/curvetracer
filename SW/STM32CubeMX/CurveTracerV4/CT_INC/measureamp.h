/*
 * measureamp.h
 *
 *  Created on: 2018. aug. 25.
 *      Author: zoli
 */

#ifndef MEASUREAMP_H_
#define MEASUREAMP_H_

#define I2C_ADDR_MEASUREAMP 0x40

#define MA_CURRENT_NUMRANGES 3
#define MA_VOLTAGE_NUMRANGES 5

/*
#define MA_CURRENT_x01	3
#define MA_CURRENT_x1	1
#define MA_CURRENT_x10	0
*/

#define MA_CURRENT_x01	0
#define MA_CURRENT_x1	2
#define MA_CURRENT_x10	3
#define MA_CURRENT_MASK	3

/*
#define MA_VOLTAGE_x01	0x38
#define MA_VOLTAGE_x1	0x30
#define MA_VOLTAGE_x10	0x34
#define MA_VOLTAGE_x100	0x24
#define MA_VOLTAGE_x1000	0x04
*/

#define MA_VOLTAGE_x01	0x04
#define MA_VOLTAGE_x1	0x0C
#define MA_VOLTAGE_x10	0x08
#define MA_VOLTAGE_x100	0x18
#define MA_VOLTAGE_x1000	0x38
#define MA_VOLTAGE_MASK 0x3C

#define MA_SEL_CURR_AUTO 0xFF
#define MA_SEL_CURR_x01 0
#define MA_SEL_CURR_x1	1
#define MA_SEL_CURR_x10	2

#define MA_SEL_VOLT_AUTO 0xFF
#define MA_SEL_VOLT_x01	0
#define MA_SEL_VOLT_x1	1
#define MA_SEL_VOLT_x10	2
#define MA_SEL_VOLT_x100	3
#define MA_SEL_VOLT_x1000	4

#define MA_FLAG_UC	1	// Under Current
#define MA_FLAG_OC  2	// Over Current
#define MA_FLAG_UV  4	// Under Voltage
#define MA_FLAG_OV	8	// Over Voltage

extern uint8_t MA_FLAGS;

void MA_Range_Switcher();
void MA_SelCurr(uint8_t index);
void MA_SelVolt(uint8_t index);
void MA_Init();

#endif /* MEASUREAMP_H_ */
