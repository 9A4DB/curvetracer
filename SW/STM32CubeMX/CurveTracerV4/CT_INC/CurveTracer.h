/*
 * CurveTracer.h
 *
 *  Created on: 2018. aug. 19.
 *      Author: zoli
 */

#ifndef CURVETRACER_H_
#define CURVETRACER_H_


// Non-Volatile settings. Saved across reboots
typedef struct
{
	uint16_t wfgNumSamples;		// number of samples in a sine wave period
	uint16_t wfgCalNullpoint;	// calibration null point
	uint16_t wfgCalSwing;		// sine wave swing from the minimum to the maximum voltage in numbers
	uint16_t wfgTimePeriod;		// timer autoreload value determine the frequency: 84MHz / wfgNumNmples / (wfgTimePeriod + 1)
	uint8_t paWiper;			// Wiper level of the digipot on the power amplifier
	uint16_t attResistanceSet;	// Set bits of the resistor network
	uint8_t attDUTSel;			// Device Under test selection
	uint16_t attDUTaltcycles;	// DUT alternation frequency
	uint8_t maCurrSel;			// Current amplification selection	(Auto, x0.1, x1, x10)
	uint8_t maVoltSel;			// Current amplification selection	(Auto, x0.1, x1, x10, x100, x1000)
} CT_NV_SettingsTypeDef;

// Volatile settings. Not saved across reboots
typedef struct
{
	double wfgFreq;	// Current Frequency
	double paVpp;	// Current Vpp setting at the power amplifier output
	double attResistance; // Current Resistance
	uint8_t attCurrentDUT;	// Currently selected DUT - In alternating mode, it changes while the active DUT changes
	uint8_t maCurrRange;
	uint8_t maVoltRange;
	uint8_t maFlags;		// MeasureAmp - under/over voltage/current flags - used by the automatic range switching
} CT_V_SettingsTypeDef;

extern CT_NV_SettingsTypeDef CT_NV_Settings;
extern CT_V_SettingsTypeDef CT_V_Settings;

void CT_init();
void CT_worker();
void CT_NV_Load();
void CT_NV_Save();

#endif /* CURVETRACER_H_ */
