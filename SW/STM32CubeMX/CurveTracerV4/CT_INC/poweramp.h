/*
 * poweramp.h
 *
 *  Created on: 2018. aug. 26.
 *      Author: zoli
 */

#ifndef POWERAMP_H_
#define POWERAMP_H_

#define I2C_ADDR_PA_DIGIPOT 0x78

double PA_SetWiper(uint8_t wiper);
void PA_CMD_SetWiper(uint8_t source, void* args[], uint8_t len);
double PA_SetVpp(double Vpp);
void PA_CMD_SetVpp(uint8_t source, void* args[], uint8_t len);
double PA_SelVpp(uint8_t index);
void PA_CMD_SelVpp(uint8_t source, void* args[], uint8_t len);
void PA_Init();
double PA_GetVpp();

#endif /* POWERAMP_H_ */
