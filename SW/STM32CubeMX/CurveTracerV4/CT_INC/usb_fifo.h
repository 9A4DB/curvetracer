/*
 * usb_fifo.h
 *
 *  Created on: 2018. aug. 22.
 *      Author: zoli
 */

#ifndef USB_FIFO_H_
#define USB_FIFO_H_

#include <stdbool.h>
#include <stdint.h>

#define FIFO_SIZE 128  // must be 2^N

#define FIFO_INCR(x) (((x)+1)&((FIFO_SIZE)-1))
#define USB_CDC_TIMEOUT 500

/* Structure of FIFO*/

typedef struct FIFO

{
uint32_t head;
uint32_t tail;
    uint8_t data[FIFO_SIZE];
} FIFO;

extern uint8_t USB_CDC_RX_numlines;
extern bool USB_CDC_Enabled;

// extern FIFO USB_RX_FIFO;

bool USB_CDC_RX_full();
void USB_CDC_RX_Enqueue(uint8_t value);
uint8_t USB_CDC_RX_read(uint8_t* Buf, uint32_t Len);
uint8_t USB_CDC_RX_readln(uint8_t* Buf);
void USB_CDC_RX_WaitforReady();
void USB_CDC_TX_write(uint8_t* Buf,uint16_t len);
void USB_CDC_Init();



#endif /* USB_FIFO_H_ */
