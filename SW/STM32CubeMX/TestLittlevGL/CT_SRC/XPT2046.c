/**
 * @file XPT2046.c
 *
 */

/*********************
 *      INCLUDES
 *********************/
#include <XPT2046.h>

#include <stddef.h>
#include "gpio.h"
#include "spi.h"
// #include LV_DRV_INDEV_INCLUDE
// #include LV_DRV_DELAY_INCLUDE

/*********************
 *      DEFINES
 *********************/


/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/
static void XPT2046_corr(int16_t * x, int16_t * y);
static void XPT2046_avg(int16_t * x, int16_t * y);
static void XPT2046_SpiCS(uint16_t value);
static uint16_t XPT2046_ReadCoordinate(uint8_t cmd);

#define XPT2046_IrqRead HAL_GPIO_ReadPin(TP_IRQ_GPIO_Port, TP_IRQ_Pin)

/**********************
 *  STATIC VARIABLES
 **********************/
int16_t avg_buf_x[XPT2046_AVG];
int16_t avg_buf_y[XPT2046_AVG];
uint8_t avg_last;

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/


static int16_t last_x = 0;
static int16_t last_y = 0;

/**
 * Get the current position and state of the touchpad
 * @param data store the read data here
 * @return false: because no ore data to be read
 */
bool XPT2046_Read(lv_indev_data_t * data)
{
    bool valid = true;

    int16_t x = 0;
    int16_t y = 0;

    uint8_t irq = XPT2046_IrqRead;

    if(irq == 0)
    {
        XPT2046_SpiCS(0);
        // HAL_Delay(1);
        x = XPT2046_ReadCoordinate(CMD_X_READ);
        y = XPT2046_ReadCoordinate(CMD_Y_READ);

        // Normalize Data
        // XPT2046_corr(&x, &y);
        // XPT2046_avg(&x, &y);

        last_x = x;
        last_y = y;
        XPT2046_SpiCS(1);
    }
    else
    {
        x = last_x;
        y = last_y;
        avg_last = 0;
        valid = false;
    }

    data->point.x = x;
    data->point.y = y;
    data->state = valid == false ? LV_INDEV_STATE_REL : LV_INDEV_STATE_PR;

    return valid;
}

/**********************
 *   STATIC FUNCTIONS
 **********************/
static void XPT2046_corr(int16_t * x, int16_t * y)
{
#if XPT2046_XY_SWAP != 0
    int16_t swap_tmp;
    swap_tmp = *x;
    *x = *y;
    *y = swap_tmp;
#endif

    if((*x) > XPT2046_X_MIN)(*x) -= XPT2046_X_MIN;
    else(*x) = 0;

    if((*y) > XPT2046_Y_MIN)(*y) -= XPT2046_Y_MIN;
    else(*y) = 0;

    (*x) = (uint32_t)((uint32_t)(*x) * XPT2046_HOR_RES) /
           (XPT2046_X_MAX - XPT2046_X_MIN);

    (*y) = (uint32_t)((uint32_t)(*y) * XPT2046_VER_RES) /
           (XPT2046_Y_MAX - XPT2046_Y_MIN);

#if XPT2046_X_INV != 0
    (*x) =  XPT2046_HOR_RES - (*x);
#endif

#if XPT2046_Y_INV != 0
    (*y) =  XPT2046_VER_RES - (*y);
#endif


}


static void XPT2046_avg(int16_t * x, int16_t * y)
{
    /*Shift out the oldest data*/
    uint8_t i;
    for(i = XPT2046_AVG - 1; i > 0 ; i--) {
        avg_buf_x[i] = avg_buf_x[i - 1];
        avg_buf_y[i] = avg_buf_y[i - 1];
    }

    /*Insert the new point*/
    avg_buf_x[0] = *x;
    avg_buf_y[0] = *y;
    if(avg_last < XPT2046_AVG) avg_last++;

    /*Sum the x and y coordinates*/
    int32_t x_sum = 0;
    int32_t y_sum = 0;
    for(i = 0; i < avg_last ; i++) {
        x_sum += avg_buf_x[i];
        y_sum += avg_buf_y[i];
    }

    /*Normalize the sums*/
    (*x) = (int32_t)x_sum / avg_last;
    (*y) = (int32_t)y_sum / avg_last;
}

// SPI Functions

static void XPT2046_SpiCS(uint16_t value)
{
	HAL_GPIO_WritePin(TP_CS_GPIO_Port, TP_CS_Pin,value);
}


static uint16_t XPT2046_ReadCoordinate(uint8_t cmd)
{
    uint8_t rx_buff[2];
    // Send command
	while (hspi2.State != HAL_SPI_STATE_READY);
    HAL_SPI_Transmit(&hspi2, &cmd, 1, 500);
    HAL_Delay(1);
    // Read response
    while (hspi2.State != HAL_SPI_STATE_READY);
    HAL_SPI_Receive(&hspi2, &rx_buff, 2, 500);
    return (rx_buff[0] << 4) + (rx_buff[1] >> 4);
}

