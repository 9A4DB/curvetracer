EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L conn:Conn_02x17_Odd_Even J1
U 1 1 5BE76E6F
P 2175 2200
F 0 "J1" H 2225 3217 50  0000 C CNN
F 1 "LCD34" H 2225 3126 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x17_Pitch2.54mm" H 2175 2200 50  0001 C CNN
F 3 "~" H 2175 2200 50  0001 C CNN
	1    2175 2200
	1    0    0    -1  
$EndComp
NoConn ~ 1975 1400
$Comp
L power:GND #PWR02
U 1 1 5BE76F3E
P 2575 3075
F 0 "#PWR02" H 2575 2825 50  0001 C CNN
F 1 "GND" H 2580 2902 50  0000 C CNN
F 2 "" H 2575 3075 50  0001 C CNN
F 3 "" H 2575 3075 50  0001 C CNN
	1    2575 3075
	1    0    0    -1  
$EndComp
Wire Wire Line
	2475 1400 2575 1400
Wire Wire Line
	2575 1400 2575 2600
Wire Wire Line
	2475 3000 2575 3000
Connection ~ 2575 3000
Wire Wire Line
	2575 3000 2575 3075
Wire Wire Line
	2475 2600 2575 2600
Connection ~ 2575 2600
Wire Wire Line
	2575 2600 2575 3000
NoConn ~ 2475 2500
$Comp
L power:+3.3V #PWR01
U 1 1 5BE76F9E
P 1875 1300
F 0 "#PWR01" H 1875 1150 50  0001 C CNN
F 1 "+3.3V" H 1890 1473 50  0000 C CNN
F 2 "" H 1875 1300 50  0001 C CNN
F 3 "" H 1875 1300 50  0001 C CNN
	1    1875 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1875 1300 1875 2600
Wire Wire Line
	1875 3000 1975 3000
Wire Wire Line
	1975 2600 1875 2600
Connection ~ 1875 2600
Wire Wire Line
	1875 2600 1875 3000
Text GLabel 1800 1500 0    50   Input ~ 0
D0
Text GLabel 1800 1600 0    50   Input ~ 0
D2
Text GLabel 1800 1700 0    50   Input ~ 0
D4
Text GLabel 1800 1800 0    50   Input ~ 0
D6
Text GLabel 1800 1900 0    50   Input ~ 0
D8
Text GLabel 1800 2000 0    50   Input ~ 0
D10
Text GLabel 1800 2100 0    50   Input ~ 0
D12
Text GLabel 1800 2200 0    50   Input ~ 0
D14
Text GLabel 2650 1500 2    50   Input ~ 0
D1
Text GLabel 2650 1600 2    50   Input ~ 0
D3
Text GLabel 2650 1700 2    50   Input ~ 0
D5
Text GLabel 2650 1800 2    50   Input ~ 0
D7
Text GLabel 2650 1900 2    50   Input ~ 0
D9
Text GLabel 2650 2000 2    50   Input ~ 0
D11
Text GLabel 2650 2100 2    50   Input ~ 0
D13
Text GLabel 2650 2200 2    50   Input ~ 0
D15
Text GLabel 2650 2300 2    50   Input ~ 0
LCD_RS
Text GLabel 2650 2400 2    50   Input ~ 0
LCD_RD
Text GLabel 2675 2700 2    50   Input ~ 0
TP_IRQ
Text GLabel 2675 2800 2    50   Input ~ 0
TP_CLK
Text GLabel 2675 2900 2    50   Input ~ 0
TP_DOUT
Text GLabel 1800 2800 0    50   Input ~ 0
TP_CS
Text GLabel 1800 2900 0    50   Input ~ 0
TP_DIN
Text GLabel 1800 2300 0    50   Input ~ 0
LCD_CS
Text GLabel 1800 2400 0    50   Input ~ 0
LCD_WR
Wire Wire Line
	1800 1500 1975 1500
Wire Wire Line
	1800 1600 1975 1600
Wire Wire Line
	1800 1700 1975 1700
Wire Wire Line
	1800 1800 1975 1800
Wire Wire Line
	1800 1900 1975 1900
Wire Wire Line
	1800 2000 1975 2000
Wire Wire Line
	1800 2100 1975 2100
Wire Wire Line
	1800 2200 1975 2200
Wire Wire Line
	1800 2300 1975 2300
Wire Wire Line
	1800 2400 1975 2400
Wire Wire Line
	1800 2800 1975 2800
Wire Wire Line
	1800 2900 1975 2900
Wire Wire Line
	2475 1500 2650 1500
Wire Wire Line
	2475 1600 2650 1600
Wire Wire Line
	2475 1700 2650 1700
Wire Wire Line
	2475 1800 2650 1800
Wire Wire Line
	2475 1900 2650 1900
Wire Wire Line
	2475 2000 2650 2000
Wire Wire Line
	2475 2100 2650 2100
Wire Wire Line
	2475 2200 2650 2200
Wire Wire Line
	2475 2300 2650 2300
Wire Wire Line
	2475 2400 2650 2400
Wire Wire Line
	2475 2700 2675 2700
Wire Wire Line
	2475 2800 2675 2800
Wire Wire Line
	2475 2900 2675 2900
$Comp
L conn:Conn_02x20_Odd_Even J2
U 1 1 5BE7BC69
P 4025 2300
F 0 "J2" H 4075 3417 50  0000 C CNN
F 1 "LCD40" H 4075 3326 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x20_Pitch2.54mm" H 4025 2300 50  0001 C CNN
F 3 "~" H 4025 2300 50  0001 C CNN
	1    4025 2300
	1    0    0    -1  
$EndComp
Text GLabel 4525 1400 2    50   Input ~ 0
D0
NoConn ~ 3825 3300
NoConn ~ 4325 3300
NoConn ~ 4325 3200
Text GLabel 4525 1500 2    50   Input ~ 0
D1
Text GLabel 4525 1600 2    50   Input ~ 0
D2
Text GLabel 4525 1700 2    50   Input ~ 0
D3
Text GLabel 4525 1800 2    50   Input ~ 0
D4
Text GLabel 4525 1900 2    50   Input ~ 0
D5
Text GLabel 4525 2000 2    50   Input ~ 0
D6
Text GLabel 4525 2100 2    50   Input ~ 0
D7
Text GLabel 4525 2200 2    50   Input ~ 0
TP_CLK
Text GLabel 4525 2300 2    50   Input ~ 0
TP_CS
Text GLabel 4525 2400 2    50   Input ~ 0
TP_DIN
NoConn ~ 4325 2500
Text GLabel 4525 2600 2    50   Input ~ 0
TP_DOUT
Text GLabel 4525 2700 2    50   Input ~ 0
TP_IRQ
$Comp
L power:GND #PWR04
U 1 1 5BE7D75C
P 3750 3375
F 0 "#PWR04" H 3750 3125 50  0001 C CNN
F 1 "GND" H 3755 3202 50  0000 C CNN
F 2 "" H 3750 3375 50  0001 C CNN
F 3 "" H 3750 3375 50  0001 C CNN
	1    3750 3375
	1    0    0    -1  
$EndComp
Wire Wire Line
	3825 1400 3750 1400
Wire Wire Line
	3750 1400 3750 3375
$Comp
L power:+3.3V #PWR03
U 1 1 5BE7DEDB
P 3700 1325
F 0 "#PWR03" H 3700 1175 50  0001 C CNN
F 1 "+3.3V" H 3715 1498 50  0000 C CNN
F 2 "" H 3700 1325 50  0001 C CNN
F 3 "" H 3700 1325 50  0001 C CNN
	1    3700 1325
	1    0    0    -1  
$EndComp
Wire Wire Line
	3825 1500 3700 1500
Wire Wire Line
	3700 1500 3700 1325
NoConn ~ 3825 1600
Text GLabel 3675 1700 0    50   Input ~ 0
LCD_RS
Text GLabel 3675 1800 0    50   Input ~ 0
LCD_WR
Text GLabel 3675 1900 0    50   Input ~ 0
LCD_RD
Text GLabel 3675 2000 0    50   Input ~ 0
D8
Text GLabel 3675 2100 0    50   Input ~ 0
D9
Text GLabel 3675 2200 0    50   Input ~ 0
D10
Text GLabel 3675 2300 0    50   Input ~ 0
D11
Text GLabel 3675 2400 0    50   Input ~ 0
D12
Text GLabel 3675 2500 0    50   Input ~ 0
D13
Text GLabel 3675 2600 0    50   Input ~ 0
D14
Text GLabel 3675 2700 0    50   Input ~ 0
D15
Text GLabel 3675 2800 0    50   Input ~ 0
LCD_CS
Text GLabel 3675 2900 0    50   Input ~ 0
LCD_RST
Text GLabel 1800 2500 0    50   Input ~ 0
LCD_RST
Text GLabel 1800 2700 0    50   Input ~ 0
BL_PWM
Text GLabel 5450 1650 0    50   Input ~ 0
BL_PWM
Wire Wire Line
	4325 1400 4525 1400
Wire Wire Line
	4325 1500 4525 1500
Wire Wire Line
	4325 1600 4525 1600
Wire Wire Line
	4325 1700 4525 1700
Wire Wire Line
	4325 1800 4525 1800
Wire Wire Line
	4325 1900 4525 1900
Wire Wire Line
	4325 2000 4525 2000
Wire Wire Line
	4325 2100 4525 2100
Wire Wire Line
	4325 2200 4525 2200
Wire Wire Line
	4325 2300 4525 2300
Wire Wire Line
	4325 2400 4525 2400
Wire Wire Line
	4325 2600 4525 2600
Wire Wire Line
	4325 2700 4525 2700
Wire Wire Line
	3675 1700 3825 1700
Wire Wire Line
	3675 1800 3825 1800
Wire Wire Line
	3675 1900 3825 1900
Wire Wire Line
	3675 2000 3825 2000
Wire Wire Line
	3675 2100 3825 2100
Wire Wire Line
	3675 2200 3825 2200
Wire Wire Line
	3675 2300 3825 2300
Wire Wire Line
	3675 2400 3825 2400
Wire Wire Line
	3675 2500 3825 2500
Wire Wire Line
	3675 2600 3825 2600
Wire Wire Line
	3675 2700 3825 2700
Wire Wire Line
	3675 2800 3825 2800
Wire Wire Line
	3675 2900 3825 2900
Wire Wire Line
	3675 3200 3825 3200
Wire Wire Line
	1975 2500 1800 2500
Wire Wire Line
	1975 2700 1800 2700
Text GLabel 4525 2800 2    50   Input ~ 0
MISO
Text GLabel 4525 2900 2    50   Input ~ 0
CLK
Text GLabel 4525 3000 2    50   Input ~ 0
MOSI
Text GLabel 4525 3100 2    50   Input ~ 0
SD_CS
Text GLabel 3675 3000 0    50   Input ~ 0
FLASH_CS
Wire Wire Line
	3675 3000 3825 3000
Wire Wire Line
	4325 2800 4525 2800
Wire Wire Line
	4325 2900 4525 2900
Wire Wire Line
	4325 3000 4525 3000
Wire Wire Line
	4325 3100 4525 3100
$Comp
L Device:Jumper_NC_Dual JP1
U 1 1 5BEA4AAE
P 5650 1400
F 0 "JP1" V 5604 1501 50  0000 L CNN
F 1 "BACKLIGHT" V 5695 1501 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 5650 1400 50  0001 C CNN
F 3 "~" H 5650 1400 50  0001 C CNN
	1    5650 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 1400 5550 1400
Text GLabel 3675 3200 0    50   Input ~ 0
LCD_BL
Text GLabel 5450 1400 0    50   Input ~ 0
LCD_BL
$Comp
L power:+3.3V #PWR06
U 1 1 5BEA6A94
P 5650 1075
F 0 "#PWR06" H 5650 925 50  0001 C CNN
F 1 "+3.3V" H 5665 1248 50  0000 C CNN
F 2 "" H 5650 1075 50  0001 C CNN
F 3 "" H 5650 1075 50  0001 C CNN
	1    5650 1075
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1075 5650 1150
Wire Wire Line
	5450 1650 5650 1650
$Comp
L conn:Conn_01x06 J3
U 1 1 5BEAAB82
P 5875 2075
F 0 "J3" H 5955 2067 50  0000 L CNN
F 1 "SPI" H 5955 1976 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 5875 2075 50  0001 C CNN
F 3 "~" H 5875 2075 50  0001 C CNN
	1    5875 2075
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5BEAAC02
P 5600 2450
F 0 "#PWR05" H 5600 2200 50  0001 C CNN
F 1 "GND" H 5605 2277 50  0000 C CNN
F 2 "" H 5600 2450 50  0001 C CNN
F 3 "" H 5600 2450 50  0001 C CNN
	1    5600 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5675 2375 5600 2375
Wire Wire Line
	5600 2375 5600 2450
Text GLabel 5550 1975 0    50   Input ~ 0
SD_CS
Text GLabel 5550 1875 0    50   Input ~ 0
FLASH_CS
Text GLabel 5550 2275 0    50   Input ~ 0
CLK
Text GLabel 5550 2075 0    50   Input ~ 0
MISO
Text GLabel 5550 2175 0    50   Input ~ 0
MOSI
Wire Wire Line
	5550 1875 5675 1875
Wire Wire Line
	5550 1975 5675 1975
Wire Wire Line
	5550 2075 5675 2075
Wire Wire Line
	5550 2175 5675 2175
Wire Wire Line
	5550 2275 5675 2275
$Comp
L power:+5V #PWR07
U 1 1 5BEBCF80
P 3175 2975
F 0 "#PWR07" H 3175 2825 50  0001 C CNN
F 1 "+5V" H 3190 3148 50  0000 C CNN
F 2 "" H 3175 2975 50  0001 C CNN
F 3 "" H 3175 2975 50  0001 C CNN
	1    3175 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	3825 3100 3175 3100
Wire Wire Line
	3175 3100 3175 2975
$Comp
L power:+5V #PWR08
U 1 1 5BEBF691
P 5300 2800
F 0 "#PWR08" H 5300 2650 50  0001 C CNN
F 1 "+5V" H 5315 2973 50  0000 C CNN
F 2 "" H 5300 2800 50  0001 C CNN
F 3 "" H 5300 2800 50  0001 C CNN
	1    5300 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5BEBF6BB
P 5300 3075
F 0 "#PWR09" H 5300 2825 50  0001 C CNN
F 1 "GND" H 5305 2902 50  0000 C CNN
F 2 "" H 5300 3075 50  0001 C CNN
F 3 "" H 5300 3075 50  0001 C CNN
	1    5300 3075
	1    0    0    -1  
$EndComp
$Comp
L conn:Conn_01x02 J4
U 1 1 5BEBF751
P 5600 2875
F 0 "J4" H 5679 2867 50  0000 L CNN
F 1 "BL_PWR" H 5679 2776 50  0000 L CNN
F 2 "suf_connector_ncw:CONN_NCW254-02R" H 5600 2875 50  0001 C CNN
F 3 "~" H 5600 2875 50  0001 C CNN
	1    5600 2875
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 2875 5300 2875
Wire Wire Line
	5300 2875 5300 2800
Wire Wire Line
	5300 2975 5300 3075
Wire Wire Line
	5300 2975 5400 2975
$EndSCHEMATC
