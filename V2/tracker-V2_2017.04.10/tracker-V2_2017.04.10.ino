#include <Adafruit_GFX.h> // Hardware-specific library
#include <MCUFRIEND_kbv.h>
#include <SPI.h>
MCUFRIEND_kbv tft;

#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define GREY    0xBE5A
//------------------------------------
#define Navy            0x000F      /*   0,   0, 128 */
#define DarkGreen       0x03E0      /*   0, 128,   0 */
#define DarkCyan        0x03EF      /*   0, 128, 128 */
#define Maroon          0x7800      /* 128,   0,   0 */
#define Purple          0x780F      /* 128,   0, 128 */
#define Olive           0x7BE0      /* 128, 128,   0 */
#define LightGrey       0xC618      /* 192, 192, 192 */
#define DarkGrey        0x7BEF      /* 128, 128, 128 */
#define Orange          0xFD20      /* 255, 165,   0 */
#define GreenYellow     0xAFE5      /* 173, 255,  47 */
#define Pink            0xF81F
//--------------------------------Leds----------------------------------------------
#define FLED 23
#define RLED 25
#define VLED 27
#define ABLED 29
#define MLED 31
#define UPLED 33
#define DWLED 35
//-------------------------------Buttons---------------------------------------
#define FBUTTON 22
#define RBUTTON 24
#define VBUTTON 26
#define ABBUTTON 28
#define MBUTTON 30
#define UPBUTTON 32
#define DWBUTTON 34
//------------------------------Relays----------------------------------------------
#define RELAY1 36
#define RELAY2 37
#define RELAY3 38
#define RELAY4 39
//------------------------------------Variables-------------------------------------
int a = 0;
int b = 0;
int c = 0;

int d1 = 0;
int d2 = 0;
int d3 = 0;
int d4 = 0;
int d32 = 0;
int d34 = 0;
int d28 = 0;

int e = 0;
int t = 0;

int buttonState = 0;
int range = 0;
int M1 = 0;
int M2 = 0;
int M3 = 0;
int M4 = 0;

int V = 25;
int R = 1;
int F = 1;

int result;
int updown = 0;
int modeABL = 1;

int BUTTONS[] = {
  0, 0, 0, 0, 0, 0, 0
};

const int analogInPinV = A8;
const int analogInPinI = A10;
//--------------------------------------------
int CH1_value1[260];
int CH1_value2[260];
int CH1_value1D[260];// delete
int CH1_value2D[260];
//--------------------------------------------
int CH2_value1[260];
int CH2_value2[260];
int CH2_value1D[260];// delete
int CH2_value2D[260];
//-------------------------------------------
int CH1M_value1[400];
int CH1M_value2[400];
int CH1M_value1D[400];// delete
int CH1M_value2D[400];
//--------------------------------------------
int CH2M_value1[400];
int CH2M_value2[400];
int CH2M_value1D[400];// delete
int CH2M_value2D[400];

int max1 = 0;
int max1_reset = 0;
int min1_reset = 0;
int min1 = 0;
int max1_t = 0;
int min1_t = 0;

int i = 0;
int outputValue1 = 0;
int outputValue2 = 0;

int freqvalue = 0;

const int CS = 42;
const int CSV = 43;
const int CSI = 41;
int level = 0;


int Delay = 0;
int DelayT = 0;

int Trig = 0;

//-----------------------------------------ARRAY - Volt--------------------------------------------
//-------y-----x-----

int VOLT[ 5 ][ 25 ] =
{ { 255, 253, 251, 249, 247, 245, 243, 241, 240, 238, 236, 234, 232, 231, 229, 227, 225, 224, 222, 220, 218, 216, 214, 213, 211},
  { 253, 250, 246, 243, 239, 235, 232, 229, 226, 223, 219, 216, 213, 210, 207, 204, 202, 199, 196, 192, 188, 185, 182, 178, 174},
  { 253, 248, 244, 240, 236, 232, 228, 224, 220, 216, 213, 209, 205, 202, 199, 194, 190, 186, 182, 179, 175, 171, 166, 158, 155},
  { 250, 244, 238, 232, 226, 220, 215, 209, 203, 199, 192, 186, 181, 177, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  { 249, 241, 234, 227, 219, 212, 206, 198, 191, 186, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
};

//-----------------------------------------ARRAY - AMP--------------------------------------------
//-------y-----x-----

int AMP[ 5 ][ 25 ] =
{ { 0, 255, 255, 255, 255, 255, 255, 255, 255, 255, 254, 254, 253, 253, 253, 252, 252, 252, 251, 251, 251, 250, 250, 249, 249},
  { 255, 255, 255, 254, 253, 252, 251, 250, 250, 249, 248, 247, 246, 245, 244, 243, 242, 242, 241, 240, 239, 238, 237, 235, 235},
  { 255, 255, 254, 253, 251, 250, 248, 247, 246, 245, 243, 242, 241, 240, 238, 237, 236, 235, 233, 232, 231, 230, 228, 227, 225},
  { 253, 247, 242, 236, 231, 226, 221, 216, 212, 207, 202, 197, 193, 189, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
};

//----------------------------------------Start the setup loop-------------------------------
void setup()
{

  Set();
  REG_ADC_MR = (REG_ADC_MR & 0xFFF0FFFF) | 0x00020000;
  pinMode (CS, OUTPUT);
  pinMode (CSV, OUTPUT);
  pinMode (CSI, OUTPUT);
  SPI.begin();
  analogWrite(DAC0, 100 );
  tft.reset();
  uint16_t id = tft.readID();
  tft.begin(id);
  tft.setRotation(1);

  Serial.begin(38400);

  Serial.println("CLEARDATA");

  Serial.println("LABEL,X,Y");

}
//-------------------------------------------Starting the main loop---------------------------
void loop()
{

  RELAY();
  switch (menubutton()) {
    case 0:         //---------------------------MAIN--------------------
      mainscreen();
      modeABM();
      mainline();
      digitalWrite(MLED, LOW);
      digitalWrite(VLED, LOW);
      digitalWrite(RLED, LOW);
      digitalWrite(FLED, LOW);
      digitalWrite(UPLED, LOW);
      digitalWrite(DWLED, LOW);

      break;

    case 1:        //----------------------------MENU--------------------
      menu();
      modeAB();
      menu_line();
      save();
      digitalWrite(MLED, HIGH);
      digitalWrite(VLED, LOW);
      digitalWrite(RLED, LOW);
      digitalWrite(FLED, LOW);
      digitalWrite(UPLED, LOW);
      digitalWrite(DWLED, LOW);

      //delay(2000);
      break;


    case 2:        //----------------------------MENUV--------------------


      modeAB();
      menu_line();
      level = map(V, 1, 25, 10, 255);
      MCP41010Write(level);

      MCP41010WriteV(VOLT[R - 1][V - 1]);

      MCP41010WriteI(AMP[R - 1][V - 1]);


      tft.setCursor(400, 70);
      tft.setTextColor(GREY);  tft.setTextSize(3);
      tft.println(V);
      V = V + updownbutton();
      if (V <= 1)
      {
        V = 1;
        digitalWrite(UPLED, HIGH);
        digitalWrite(DWLED, LOW);
      }
      if (V >= 25)
      {
        V = 25;
        digitalWrite(UPLED, LOW);
        digitalWrite(DWLED, HIGH);
      }
      if (2 <= V && V <= 24)
      {
        digitalWrite(UPLED, HIGH);
        digitalWrite(DWLED, HIGH);
      }
      menu();
      digitalWrite(VLED, HIGH);
      digitalWrite(MLED, LOW);
      digitalWrite(RLED, LOW);
      digitalWrite(FLED, LOW);

      tft.setCursor(355, 70);
      tft.setTextColor(BLACK);  tft.setTextSize(3);
      tft.println("V:");

      tft.setCursor(400, 70);
      tft.setTextColor(RED);  tft.setTextSize(3);
      tft.println(V);
      tft.drawLine(355, 95, 475, 95, BLACK);//----
      tft.drawLine(355, 96, 475, 96, BLACK);//----


      //delay(2000);
      break;
    case 3:        //----------------------------MENUR--------------------

      modeAB();
      menu_line();

      MCP41010WriteV(VOLT[R - 1][V - 1]);

      MCP41010WriteI(AMP[R - 1][V - 1]);

      tft.setCursor(400, 105);
      tft.setTextColor(GREY);  tft.setTextSize(3);
      tft.println(R);
      R = R + updownbutton();
      if (R <= 1)
      {
        R = 1;
        digitalWrite(UPLED, HIGH);
        digitalWrite(DWLED, LOW);
      }
      if (R >= 5)
      {
        R = 5;
        digitalWrite(UPLED, LOW);
        digitalWrite(DWLED, HIGH);
      }
      if (2 <= R && R <= 4)
      {
        digitalWrite(UPLED, HIGH);
        digitalWrite(DWLED, HIGH);
      }
      menu();
      digitalWrite(RLED, HIGH);
      digitalWrite(MLED, LOW);
      digitalWrite(FLED, LOW);
      digitalWrite(VLED, LOW);

      tft.setCursor(355, 105);
      tft.setTextColor(BLACK);  tft.setTextSize(3);
      tft.println("R:");

      tft.setCursor(400, 105);
      tft.setTextColor(RED);  tft.setTextSize(3);
      tft.println(R);
      tft.drawLine(355, 130, 475, 130, BLACK);//----
      tft.drawLine(355, 131, 475, 131, BLACK);//----

      //delay(2000);
      break;
    case 4:        //----------------------------MENUF--------------------

      modeAB();
      menu_line();
      FREQ();
      tft.setCursor(400, 140);
      tft.setTextColor(GREY);  tft.setTextSize(3);
      tft.println(F);
      F = F + updownbutton();
      if (F <= 1)
      {
        F = 1;
        Delay = 300;
        digitalWrite(UPLED, HIGH);
        digitalWrite(DWLED, LOW);
      }
      if (F >= 6)
      {
        F = 6;
        digitalWrite(UPLED, LOW);
        digitalWrite(DWLED, HIGH);
      }
      if (2 <= F && F <= 5)
      {
        digitalWrite(UPLED, HIGH);
        digitalWrite(DWLED, HIGH);
      }
      menu();
      digitalWrite(FLED, HIGH);
      digitalWrite(MLED, LOW);
      digitalWrite(VLED, LOW);
      digitalWrite(RLED, LOW);

      tft.setCursor(355, 140);
      tft.setTextColor(BLACK);  tft.setTextSize(3);
      tft.println("F:");

      tft.setCursor(400, 140);
      tft.setTextColor(RED);  tft.setTextSize(3);
      tft.println(F);
      tft.drawLine(355, 165, 475, 165, BLACK);//----
      tft.drawLine(355, 166, 475, 166, BLACK);//----

      if (F == 2)
      {
        Delay = 100;
      }
      if (F >= 3)
      {
        Delay = 0;
      }
      break;
  }
}
//-------------------------------------------------------------------------
void Set()
{
  pinMode(23, OUTPUT); // F LED
  pinMode(25, OUTPUT); // R LED
  pinMode(27, OUTPUT); // V LED
  pinMode(29, OUTPUT); // A-B LED
  pinMode(31, OUTPUT); // Menu LED
  pinMode(33, OUTPUT); // Up LED
  pinMode(35, OUTPUT); // Down LED

  pinMode(22, INPUT); // F BUTTON
  pinMode(24, INPUT); // R BUTTON
  pinMode(26, INPUT); // V BUTTON
  pinMode(28, INPUT); // A-B BUTTON
  pinMode(30, INPUT); // Menu BUTTON
  pinMode(32, INPUT); // Up BUTTON
  pinMode(34, INPUT); // Down BUTTON

  pinMode(36, OUTPUT); // RELAY1
  pinMode(37, OUTPUT); // RELAY2
  pinMode(38, OUTPUT); // RELAY3
  pinMode(39, OUTPUT); // REALY4

  pinMode(44, OUTPUT); // CH1_reed
  pinMode(45, OUTPUT); // CH2_reed

}
//---------------------------------------------Drawing the main screen---------------------------------
void mainscreen()
{
  if (e == 0)
  {
    tft.fillScreen(WHITE);
    tft.fillRect(0, 0, 480, 20, GREY); //up
    tft.fillRect(0, 20, 30, 320, GREY); //left
    tft.fillRect(450, 20, 480, 320, GREY); //right
    tft.fillRect(30, 300, 450, 320, GREY); //down
    tft.drawRect(30 , 20, 420, 280, BLACK); //rect1
    tft.drawRect(29 , 19, 422, 282, BLACK); //rect2
    //------------------------------------------------------
    tft.drawLine(240, 155, 240, 165, BLACK);//+
    tft.drawLine(235, 160, 245, 160, BLACK);//+

    tft.drawLine(235, 30, 245, 30, BLACK);//up
    tft.drawLine(240, 30, 240, 40, BLACK);

    tft.drawLine(235, 290, 245, 290, BLACK);//down
    tft.drawLine(240, 280, 240, 290, BLACK);

    tft.drawLine(440, 155, 440, 165, BLACK);//right
    tft.drawLine(430, 160, 440, 160, BLACK);

    tft.drawLine(40, 155, 40, 165, BLACK);//left
    tft.drawLine(40, 160, 50, 160, BLACK);

    tft.drawLine(90, 155, 90, 165, BLACK); //|
    tft.drawLine(140, 155, 140, 165, BLACK);
    tft.drawLine(190, 155, 190, 165, BLACK);
    tft.drawLine(290, 155, 290, 165, BLACK); //|
    tft.drawLine(340, 155, 340, 165, BLACK);
    tft.drawLine(390, 155, 390, 165, BLACK);
    e++;
  }
}
//---------------------------------------------Drawing the main screen lines-----------------------
void mainline()
{
  tft.drawLine(240, 155, 240, 165, BLACK);//+
  tft.drawLine(235, 160, 245, 160, BLACK);//+

  tft.drawLine(235, 30, 245, 30, BLACK);//up
  tft.drawLine(240, 30, 240, 40, BLACK);

  tft.drawLine(235, 290, 245, 290, BLACK);//down
  tft.drawLine(240, 280, 240, 290, BLACK);

  tft.drawLine(440, 155, 440, 165, BLACK);//right
  tft.drawLine(430, 160, 440, 160, BLACK);

  tft.drawLine(40, 155, 40, 165, BLACK);//left
  tft.drawLine(40, 160, 50, 160, BLACK);

  tft.drawLine(90, 155, 90, 165, BLACK); //|
  tft.drawLine(140, 155, 140, 165, BLACK);
  tft.drawLine(190, 155, 190, 165, BLACK);
  tft.drawLine(290, 155, 290, 165, BLACK); //|
  tft.drawLine(340, 155, 340, 165, BLACK);
  tft.drawLine(390, 155, 390, 165, BLACK);
}
//-------------------------------------------------Drawing the menu display-----------------------------
void menu()
{
  if (a == 0)
  {
    tft.fillScreen(WHITE);
    tft.fillRect(0, 0, 480, 20, GREY); //up
    tft.fillRect(0, 20, 30, 320, GREY); //left
    tft.fillRect(349, 20, 480, 320, GREY); //right
    tft.fillRect(30, 300, 450, 320, GREY); //down
    tft.drawRect(30 , 20, 318, 280, BLACK); //rect1
    tft.drawRect(29 , 19, 320, 282, BLACK); //rect2
    //-------------------------------------------------
    tft.setCursor(0, 0);
    tft.fillRect(355, 175, 120, 35, BLUE); //right
    tft.drawRect(355 , 175, 120, 35, BLACK); //rect1
    tft.drawRect(356 , 176, 118, 33, BLACK); //rect2

    tft.fillRect(355, 220, 120, 35, BLUE); //right
    tft.drawRect(355 , 220, 120, 35, BLACK); //rect1
    tft.drawRect(356 , 221, 118, 33, BLACK); //rect2

    tft.fillRect(355, 265, 120, 35, BLUE); //right
    tft.drawRect(355 , 265, 120, 35, BLACK); //rect1
    tft.drawRect(356 , 266, 118, 33, BLACK); //rect2
    //-------------------------------------------------
    tft.setCursor(0, 0);
    tft.setCursor(410, 180);
    tft.setTextColor(BLACK);  tft.setTextSize(3);
    tft.println("1");

    tft.setCursor(410, 225);
    tft.setTextColor(BLACK);  tft.setTextSize(3);
    tft.println("2");

    tft.setCursor(410, 270);
    tft.setTextColor(BLACK);  tft.setTextSize(3);
    tft.println("3");

    tft.setCursor(355, 70);
    tft.setTextColor(BLACK);  tft.setTextSize(3);
    tft.println("V:");

    tft.setCursor(400, 70);
    tft.setTextColor(BLACK);  tft.setTextSize(3);
    tft.println(V);

    tft.setCursor(355, 105);
    tft.setTextColor(BLACK);  tft.setTextSize(3);
    tft.println("R:");

    tft.setCursor(400, 105);
    tft.setTextColor(BLACK);  tft.setTextSize(3);
    tft.println(R);

    tft.setCursor(355, 140);
    tft.setTextColor(BLACK);  tft.setTextSize(3);
    tft.println("F:");

    tft.setCursor(400, 140);
    tft.setTextColor(BLACK);  tft.setTextSize(3);
    tft.println(F);

    a++;
  }
}
//---------------------------------------------------Controlling the menu system-----------------------------------
int menubutton()
{
  if (digitalRead(30) == 1 && d1 == 1)
  {
    result = 1;
    a = 0;
    d1 = 0;
    e = 0;
    if (M1 == 2)
    {
      result = 0;
      M1 = 0;
      M2 = 0;
      M3 = 0;
      M4 = 0;
    }
    M1++;
  }
  if (digitalRead(30) == 0)
  {
    d1 = 1;
  }
  if (digitalRead(26) == 1 && d2 == 1)
  {
    result = 2;
    a = 0;
    d2 = 0;
    e = 0;
    if (M2 == 2)
    {
      result = 0;
      M1 = 0;
      M2 = 0;
      M3 = 0;
      M4 = 0;
    }
    M2++;
  }
  if (digitalRead(26) == 0)
  {
    d2 = 1;
  }
  if (digitalRead(24) == 1 && d3 == 1)
  {
    result = 3;
    a = 0;
    d3 = 0;
    e = 0;
    if (M3 == 2)
    {
      result = 0;
      M1 = 0;
      M2 = 0;
      M3 = 0;
      M4 = 0;
    }
    M3++;
  }
  if (digitalRead(24) == 0)
  {
    d3 = 1;
  }
  if (digitalRead(22) == 1 && d4 == 1)
  {
    result = 4;
    a = 0;
    d4 = 0;
    e = 0;
    if (M4 == 2)
    {
      result = 0;
      M1 = 0;
      M2 = 0;
      M3 = 0;
      M4 = 0;
    }
    M4++;
  }
  if (digitalRead(22) == 0)
  {
    d4 = 1;
  }
  return result;
}
//------------------------------------------------Up-Down buttons read out-------------------------------
int updownbutton()
{
  updown = 0;
  if (digitalRead(32) == 1 && d32 == 1)
  {
    d32 = 0;
    updown = 1;

  }
  if (digitalRead(32) == 0)
  {
    d32 = 1;
  }

  if (digitalRead(34) == 1 && d34 == 1)
  {
    d34 = 0;
    updown = -1;

  }
  if (digitalRead(34) == 0)
  {
    d34 = 1;
  }

  return updown;
}
//----------------------------------------------------Alternating mode--------------------------------------------
int modeAB()
{
  if (digitalRead(28) == 1 && d28 == 1)
  {
    d28 = 0;
    modeABL = modeABL + 1;
  }
  if (digitalRead(28) == 0)
  {
    d28 = 1;
  }

  if (modeABL == 4)
  {
    modeABL = 1;
  }

  switch (modeABL)
  {
    case 1:

      digitalWrite(29, LOW);
      SAMPLE_CH1();

      break;

    case 2:

      digitalWrite(29, LOW);
      SAMPLE_CH2();

      break;

    case 3:

      digitalWrite(29, HIGH);

      SAMPLE_CH_AB();

      break;
  }

  return modeABL;
}
//----------------------------------------------------Alternating mode main--------------------------------------------
int modeABM()
{
  if (digitalRead(28) == 1 && d28 == 1)
  {
    d28 = 0;
    modeABL = modeABL + 1;
  }
  if (digitalRead(28) == 0)
  {
    d28 = 1;
  }

  if (modeABL == 5)
  {
    modeABL = 1;
  }

  switch (modeABL)
  {
    case 1:

      digitalWrite(29, LOW);
      SAMPLE_CH1M();

      break;

    case 2:

      digitalWrite(29, LOW);
      SAMPLE_CH2M();

      break;

    case 3:

      digitalWrite(29, HIGH);

      SAMPLE_CH_ABM();

      break;
    case 4:

      digitalWrite(29, LOW);
      SAMPLE_SINE();

      break;
  }

  return modeABL;
}
//----------------------------------------------------Sets the resistance ranges-------------------------------------
int RELAY()
{
  switch (R) {
    case 1:
      digitalWrite(RELAY1, LOW); digitalWrite(RELAY2, HIGH); digitalWrite(RELAY3, HIGH); digitalWrite(RELAY4, HIGH);
      break;
    case 2:
      digitalWrite(RELAY1, HIGH); digitalWrite(RELAY2, LOW); digitalWrite(RELAY3, HIGH); digitalWrite(RELAY4, HIGH);
      break;
    case 3:
      digitalWrite(RELAY1, LOW); digitalWrite(RELAY2, LOW); digitalWrite(RELAY3, HIGH); digitalWrite(RELAY4, HIGH);
      break;
    case 4:
      digitalWrite(RELAY1, HIGH); digitalWrite(RELAY2, HIGH); digitalWrite(RELAY3, LOW); digitalWrite(RELAY4, HIGH);
      break;
    case 5:
      digitalWrite(RELAY1, HIGH); digitalWrite(RELAY2, HIGH); digitalWrite(RELAY3, HIGH); digitalWrite(RELAY4, LOW);
      break;

      return result;
  }
}
//-------------------------------------------------Sets the frequancy on the atmega328-------------------------------
int FREQ()
{
  freqvalue = map(F, 1, 6, 50, 255);
  analogWrite(DAC0, freqvalue );
}
//------------------------------------------------Sets the digital potentiometers-------------------------------------
void MCP41010Write(byte value)
{
  // Note that the integer vale passed to this subroutine
  // is cast to a byte

  digitalWrite(CS, LOW);
  SPI.transfer(B00010001); // This tells the chip to set the pot
  SPI.transfer(value);     // This tells it the pot position
  digitalWrite(CS, HIGH);
}
//--------------------------------------------Sets the V pot----------------------------------------------------------
void MCP41010WriteV(byte valueV)
{
  // Note that the integer vale passed to this subroutine
  // is cast to a byte

  digitalWrite(CSV, LOW);
  SPI.transfer(B00010001); // This tells the chip to set the pot
  SPI.transfer(valueV);     // This tells it the pot position
  digitalWrite(CSV, HIGH);
}
//--------------------------------------------Sets the I pot----------------------------------------------------------
void MCP41010WriteI(byte valueI)
{
  // Note that the integer vale passed to this subroutine
  // is cast to a byte

  digitalWrite(CSI, LOW);
  SPI.transfer(B00010001); // This tells the chip to set the pot
  SPI.transfer(valueI);     // This tells it the pot position
  digitalWrite(CSI, HIGH);
}
//--------------------------------------------------Sampling A channel------------------------------------------------
int SAMPLE_CH1()
{
  digitalWrite(45, LOW);
  digitalWrite(44, HIGH);
  for (Trig = 0; Trig <= 260; Trig++)
  {
    if (analogRead(analogInPinI) == 450)
    {
      for (i = 0; i < 260; i++)               //sampling from analog pins
      {
        CH1_value1[i] = analogRead(analogInPinV);
        CH1_value2[i] = analogRead(analogInPinI);
        for (DelayT = 0; DelayT <= Delay; DelayT++);
      }

      for (i = 0; i < 260; i++)               //display
      {

        outputValue1 = map(CH1_value1D[i], 100, 800, 259, 0);
        outputValue2 = map(CH1_value2D[i], 100, 800, 259, 0);
        tft.fillCircle(outputValue2 + 60, outputValue1 + 30, 1, WHITE); //outputValue2 + 60

        outputValue1 = map(CH1_value1[i], 100, 800, 259, 0);  //Y
        outputValue2 = map(CH1_value2[i], 100, 800, 259, 0); //X
        tft.fillCircle(outputValue2 + 60, outputValue1 + 30, 1, RED);
        CH1_value1D[i] = CH1_value1[i];
        CH1_value2D[i] = CH1_value2[i];

      }
      Trig = 260;
    }
  }
}
//--------------------------------------------------Sampling main A channel------------------------------------------------
void SAMPLE_CH1M()
{
  digitalWrite(45, LOW);
  digitalWrite(44, HIGH);
  for (Trig = 0; Trig <= 400; Trig++)
  {
    if (analogRead(analogInPinI) == 450)
    {
      for (i = 0; i < 400; i++)               //sampling from analog pins
      {
        CH1M_value1[i] = analogRead(analogInPinV);//Y
        CH1M_value2[i] = analogRead(analogInPinI);//X
        for (DelayT = 0; DelayT <= Delay; DelayT++);
      }

      for (i = 0; i < 400; i++)               //display
      {

        outputValue1 = map(CH1M_value1D[i], 100, 800, 259, 0);//Y
        outputValue2 = map(CH1M_value2D[i], 100, 800, 399, 0);//X
        tft.fillCircle(outputValue2 + 40, outputValue1 + 30, 1, WHITE); //outputValue2 + 60

        outputValue1 = map(CH1M_value1[i], 100, 800, 259, 0);  //Y
        outputValue2 = map(CH1M_value2[i], 100, 800, 399, 0); //X
        tft.fillCircle(outputValue2 + 40, outputValue1 + 30, 1, RED);
        CH1M_value1D[i] = CH1M_value1[i];
        CH1M_value2D[i] = CH1M_value2[i];

      }
      Trig = 400;
    }
  }
}
//---------------------------------------------------Sampling B channel-------------------------------------------------
int SAMPLE_CH2()
{
  digitalWrite(44, LOW);
  digitalWrite(45, HIGH);
  for (Trig = 0; Trig <= 260; Trig++)
  {
    if (analogRead(analogInPinI) == 450)
    {
      for (i = 0; i < 260; i++)               //sampling from analog pins
      {
        CH2_value1[i] = analogRead(analogInPinV);
        CH2_value2[i] = analogRead(analogInPinI);
        for (DelayT = 0; DelayT <= Delay; DelayT++);
      }

      for (i = 0; i < 260; i++)               //display
      {

        outputValue1 = map(CH2_value1D[i], 100, 800, 259, 0);
        outputValue2 = map(CH2_value2D[i], 100, 800, 259, 0);
        tft.fillCircle(outputValue2 + 60, outputValue1 + 30, 1, WHITE); //outputValue2 + 60
        //tft.fillCircle(i + 60, outputValue2 + 30, 1, WHITE); //outputValue2 + 60

        outputValue1 = map(CH2_value1[i], 100, 800, 259, 0);  //Y
        outputValue2 = map(CH2_value2[i], 100, 800, 259, 0); //X
        tft.fillCircle(outputValue2 + 60, outputValue1 + 30, 1, BLUE);
        //tft.fillCircle(i + 60, outputValue2 + 30, 1, BLUE);
        CH2_value1D[i] = CH2_value1[i];
        CH2_value2D[i] = CH2_value2[i];
      }
      Trig = 260;
    }
  }
}
//---------------------------------------------------Sampling main B channel-------------------------------------------------
int SAMPLE_CH2M()
{
  digitalWrite(44, LOW);
  digitalWrite(45, HIGH);
  for (Trig = 0; Trig <= 400; Trig++)
  {
    if (analogRead(analogInPinI) == 450)
    {
      for (i = 0; i < 400; i++)               //sampling from analog pins
      {
        CH2M_value1[i] = analogRead(analogInPinV);
        CH2M_value2[i] = analogRead(analogInPinI);
        for (DelayT = 0; DelayT <= Delay; DelayT++);
      }

      for (i = 0; i < 400; i++)               //display
      {

        outputValue1 = map(CH2M_value1D[i], 100, 800, 259, 0);
        outputValue2 = map(CH2M_value2D[i], 100, 800, 399, 0);
        tft.fillCircle(outputValue2 + 40, outputValue1 + 30, 1, WHITE); //outputValue2 + 60
        //tft.fillCircle(i + 60, outputValue2 + 30, 1, WHITE); //outputValue2 + 60

        outputValue1 = map(CH2M_value1[i], 100, 800, 259, 0);  //Y
        outputValue2 = map(CH2M_value2[i], 100, 800, 399, 0); //X
        tft.fillCircle(outputValue2 + 40, outputValue1 + 30, 1, BLUE);
        //tft.fillCircle(i + 60, outputValue2 + 30, 1, BLUE);
        CH2M_value1D[i] = CH2M_value1[i];
        CH2M_value2D[i] = CH2M_value2[i];
      }
      Trig = 400;
    }
  }
}
//------------------------------------------------------Sampling A-B channels-------------------------------------
int SAMPLE_CH_AB()
{
  digitalWrite(45, LOW);
  digitalWrite(44, HIGH);
  delay(10);
  for (Trig = 0; Trig <= 260; Trig++)
  {
    if (449 <= analogRead(analogInPinI) <= 451) //450
    {
      for (i = 0; i < 260; i++)               //sampling from analog pins
      {
        CH1_value1[i] = analogRead(analogInPinV);
        CH1_value2[i] = analogRead(analogInPinI);
        for (DelayT = 0; DelayT <= Delay; DelayT++);
      }

      for (i = 0; i < 260; i++)               //display
      {

        outputValue1 = map(CH1_value1D[i], 100, 800, 259, 0);
        outputValue2 = map(CH1_value2D[i], 100, 800, 259, 0);
        tft.fillCircle(outputValue2 + 60, outputValue1 + 30, 1, WHITE); //outputValue2 + 60


        outputValue1 = map(CH1_value1[i], 100, 800, 259, 0);  //Y
        outputValue2 = map(CH1_value2[i], 100, 800, 259, 0); //X
        tft.fillCircle(outputValue2 + 60, outputValue1 + 30, 1, RED);

        CH1_value1D[i] = CH1_value1[i];
        CH1_value2D[i] = CH1_value2[i];
      }
      Trig = 260;
    }
  }
  for (Trig = 0; Trig <= 260; Trig++)
  {
    if (449 <= analogRead(analogInPinI) <= 451) //450
    {
      digitalWrite(44, LOW);
      digitalWrite(45, HIGH);
      delay(10);

      for (i = 0; i < 260; i++)               //sampling from analog pins
      {
        CH2_value1[i] = analogRead(analogInPinV);
        CH2_value2[i] = analogRead(analogInPinI);
        for (DelayT = 0; DelayT <= Delay; DelayT++);
      }

      for (i = 0; i < 260; i++)               //display
      {

        outputValue1 = map(CH2_value1D[i], 100, 800, 259, 0);
        outputValue2 = map(CH2_value2D[i], 100, 800, 259, 0);
        tft.fillCircle(outputValue2 + 60, outputValue1 + 30, 1, WHITE); //outputValue2 + 60

        outputValue1 = map(CH2_value1[i], 100, 800, 259, 0);  //Y
        outputValue2 = map(CH2_value2[i], 100, 800, 259, 0); //X
        tft.fillCircle(outputValue2 + 60, outputValue1 + 30, 1, BLUE);
        CH2_value1D[i] = CH2_value1[i];
        CH2_value2D[i] = CH2_value2[i];
      }
      Trig = 260;
    }
  }
}
//------------------------------------------------------Sampling main A-B channels-------------------------------------
int SAMPLE_CH_ABM()
{
  digitalWrite(45, LOW);
  digitalWrite(44, HIGH);
  delay(10);
  for (Trig = 0; Trig <= 400; Trig++)
  {
    if (449 <= analogRead(analogInPinI) <= 451) //450
    {
      for (i = 0; i < 400; i++)               //sampling from analog pins
      {
        CH1M_value1[i] = analogRead(analogInPinV);
        CH1M_value2[i] = analogRead(analogInPinI);
        for (DelayT = 0; DelayT <= Delay; DelayT++);
      }

      for (i = 0; i < 400; i++)               //display
      {

        outputValue1 = map(CH1M_value1D[i], 100, 800, 259, 0);
        outputValue2 = map(CH1M_value2D[i], 100, 800, 399, 0);
        tft.fillCircle(outputValue2 + 40, outputValue1 + 30, 1, WHITE); //outputValue2 + 60


        outputValue1 = map(CH1M_value1[i], 100, 800, 259, 0);  //Y
        outputValue2 = map(CH1M_value2[i], 100, 800, 399, 0); //X
        tft.fillCircle(outputValue2 + 40, outputValue1 + 30, 1, RED);

        CH1M_value1D[i] = CH1M_value1[i];
        CH1M_value2D[i] = CH1M_value2[i];
      }
      Trig = 400;
    }
  }
  for (Trig = 0; Trig <= 400; Trig++)
  {
    if (449 <= analogRead(analogInPinI) <= 451) //450
    {
      digitalWrite(44, LOW);
      digitalWrite(45, HIGH);
      delay(10);

      for (i = 0; i < 400; i++)               //sampling from analog pins
      {
        CH2M_value1[i] = analogRead(analogInPinV);
        CH2M_value2[i] = analogRead(analogInPinI);
        for (DelayT = 0; DelayT <= Delay; DelayT++);
      }

      for (i = 0; i < 400; i++)               //display
      {

        outputValue1 = map(CH2M_value1D[i], 100, 800, 259, 0);
        outputValue2 = map(CH2M_value2D[i], 100, 800, 399, 0);
        tft.fillCircle(outputValue2 + 40, outputValue1 + 30, 1, WHITE); //outputValue2 + 60

        outputValue1 = map(CH2M_value1[i], 100, 800, 259, 0);  //Y
        outputValue2 = map(CH2M_value2[i], 100, 800, 399, 0); //X
        tft.fillCircle(outputValue2 + 40, outputValue1 + 30, 1, BLUE);
        CH2M_value1D[i] = CH2M_value1[i];
        CH2M_value2D[i] = CH2M_value2[i];
      }
      Trig = 400;
    }
  }
}
//--------------------------------------------------------oscilloscope mode------------------------------------
void SAMPLE_SINE()
{
  digitalWrite(45, LOW);
  digitalWrite(44, HIGH);
  for (Trig = 0; Trig <= 400; Trig++)
  {
    if (analogRead(analogInPinI) == 450)
    {
      for (i = 0; i < 400; i++)               //sampling from analog pins
      {
        CH1M_value1[i] = analogRead(analogInPinV);//Y
        CH1M_value2[i] = analogRead(analogInPinI);//X
        for (DelayT = 0; DelayT <= Delay; DelayT++);
      }

      for (i = 0; i < 400; i++)               //display
      {

        outputValue1 = map(CH1M_value1D[i], 100, 800, 259, 0);//Y
        outputValue2 = map(CH1M_value2D[i], 100, 800, 259, 0);//X
        tft.fillCircle(i + 40, outputValue1 + 30, 1, WHITE); //outputValue2 + 60
        tft.fillCircle(i + 40, outputValue2 + 30, 1, WHITE); //outputValue2 + 60

        outputValue1 = map(CH1M_value1[i], 100, 800, 259, 0);  //Y
        outputValue2 = map(CH1M_value2[i], 100, 800, 259, 0); //X
        tft.fillCircle(i + 40, outputValue1 + 30, 1, RED);
        tft.fillCircle(i + 40, outputValue2 + 30, 1, BLUE);
        CH1M_value1D[i] = CH1M_value1[i];
        CH1M_value2D[i] = CH1M_value2[i];

      }
      Trig = 400;
    }
  }
}
//-------------------------------------------------------Menu lines---------------------------------------------
int menu_line()
{
  tft.drawLine(189, 155, 189, 165, BLACK);//+
  tft.drawLine(184, 160, 194, 160, BLACK);//+

  tft.drawLine(184, 30, 194, 30, BLACK);//up
  tft.drawLine(189, 30, 189, 40, BLACK);

  tft.drawLine(184, 290, 194, 290, BLACK);//down
  tft.drawLine(189, 280, 189, 290, BLACK);

  tft.drawLine(338, 155, 338, 165, BLACK);//right
  tft.drawLine(328, 160, 338, 160, BLACK);

  tft.drawLine(40, 155, 40, 165, BLACK);//left
  tft.drawLine(40, 160, 50, 160, BLACK);

  tft.drawLine(90, 155, 90, 165, BLACK); //|
  tft.drawLine(140, 155, 140, 165, BLACK);
  tft.drawLine(240, 155, 240, 165, BLACK);
  tft.drawLine(290, 155, 290, 165, BLACK); //|

  switch (modeABL)
  {
    case 1:

      tft.setCursor(320, 30);
      tft.setTextColor(WHITE);  tft.setTextSize(3);
      tft.println("B");

      tft.setCursor(300, 30);
      tft.setTextColor(WHITE);  tft.setTextSize(3);
      tft.println("A");
      tft.setCursor(320, 30);
      tft.setTextColor(WHITE);  tft.setTextSize(3);
      tft.println("B");

      tft.setCursor(320, 30);
      tft.setTextColor(RED);  tft.setTextSize(3);
      tft.println("A");

      break;

    case 2:

      tft.setCursor(320, 30);
      tft.setTextColor(WHITE);  tft.setTextSize(3);
      tft.println("A");

      tft.setCursor(300, 30);
      tft.setTextColor(WHITE);  tft.setTextSize(3);
      tft.println("A");
      tft.setCursor(320, 30);
      tft.setTextColor(WHITE);  tft.setTextSize(3);
      tft.println("B");

      tft.setCursor(320, 30);
      tft.setTextColor(BLUE);  tft.setTextSize(3);
      tft.println("B");

      break;

    case 3:

      tft.setCursor(320, 30);
      tft.setTextColor(WHITE);  tft.setTextSize(3);
      tft.println("A");

      tft.setCursor(320, 30);
      tft.setTextColor(WHITE);  tft.setTextSize(3);
      tft.println("B");

      tft.setCursor(300, 30);
      tft.setTextColor(RED);  tft.setTextSize(3);
      tft.println("A");
      tft.setCursor(320, 30);
      tft.setTextColor(BLUE);  tft.setTextSize(3);
      tft.println("B");

      break;
  }
}
//------------------------------------------Sendig the curve---------------------------------------------
int save()
{
  if (digitalRead(34) == 1 && d34 == 1)
  {
    d34 = 0;
    tft.setCursor(135, 200);
    tft.setTextColor(BLACK);  tft.setTextSize(3);
    tft.println("SAVING");
    Serial.println("CLEARDATA");
    for (i = 0; i < 260; i++)               //sampling from analog pins
    {
      Serial.print("DATA,");
      Serial.print(CH1_value1[i]);
      Serial.print(",");
      Serial.print(CH1_value2[i]);
      Serial.println();
    }

  }
  if (digitalRead(34) == 0)
  {
    d34 = 1;
    tft.setCursor(135, 200);
    tft.setTextColor(WHITE);  tft.setTextSize(3);
    tft.println("SAVING");
  }

}

